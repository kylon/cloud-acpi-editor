<?php
/*
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * License - http://www.gnu.org/licenses/gpl-3.0.txt
 */

echo "Building..\n";

$patch = [
    ['var Module=typeof Module!=="undefined"?Module:{};', ''],
    //['quit_(status,new ExitStatus(status))', ''],
    ['err=Module["printErr"]||console.warn.bind(console)', 'err=function(msg){if(Module["acpiedbg"])console.warn(msg)}'],
    ['err(UTF8ArrayToString', 'Module["printErr"](UTF8ArrayToString']
];
$base = file_get_contents('src/base.bjf');
$module = file_get_contents('src/iasl_emscripten_module.bjf');
$dirIter = new DirectoryIterator('src/iasl');
$fileList = [];
$iaslList = '';
$iaslFns = '';
$idx = 0;

foreach ($dirIter as $finfo) {
    if (!$finfo->isFile() || $finfo->getFilename()[0] === '.')
        continue;

    $js = file_get_contents($finfo->getPathname());

    foreach ($patch as $d)
        $js = str_replace($d[0], $d[1], $js);

    array_push($fileList, [$js, $finfo->getFilename()]);
}

/**
 * command line arguments
 *
 * sort iasl binaries bt file name (one of):
 *
 * sortasc: ascending
 * sortdsc: descending
 */
if (isset($argv[1]) && ($argv[1] === 'sortasc' || $argv[1] === 'sortdsc')) {
    echo "sorting files..";

    usort($fileList, function ($a, $b) {
        return strcasecmp($a[1], $b[1]);
    });

    if ($argv[1] === 'sortdsc')
        $fileList = array_reverse($fileList);
}

foreach ($fileList as $f) {
    $iaslList .= "'iasl{$idx}': run_iasl{$idx}, ";
    $iaslFns .= "function run_iasl{$idx}(mdata, respData, shared) {\n{$module}\n\n{$f[0]}\n}\n\n";

    ++$idx;
}

$build = str_replace(['@iaslBins', '@iaslFunctions'], [$iaslList, $iaslFns], $base);
$out = fopen('wasm-iasl.js', 'w+');

fwrite($out, $build);
fclose($out);

echo "done!\nAdding wasm-iasl.js to acpie..\n";

$ret = copy('wasm-iasl.js', __DIR__.'/../acpied/iasl/wasm/wasm-iasl.js');

if ($ret) {
    unlink('wasm-iasl.js');
    echo "Success!\n";

} else {
    echo "ERROR: unable to copy wasm-iasl.js to its destination!\nSomething went wrong or i do not have the permission to do that!\nPlease move wasm-iasl.js to acpied/iasl/wasm directory";
}
