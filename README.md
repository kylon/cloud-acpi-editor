# CLOUD ACPI EDITOR
---

## Features
   * Supported extensions: .asl, .dsl, .aml
   * Save as .dsl or .aml
   * Show iasl assembler output
   * MaciASL patches support
   * Syntax highlighting
   * Per file settings
   * Mobile-friendly

Missing/WIP:
 * Custom maciasl urls (?)
 
## ACPIE Requirements   
   * HTTP Server (Apache, Nginx, ...)
   * PHP 7.3.0+ (latest version is always recommended for better performance/security)
   * npm
   * composer
   * Any OS

### Cloud ACPI Editor Wiki

You can find setup guides and other info [here](https://bitbucket.org/kylon/cloud-acpi-editor/wiki/Home)

## How to setup ACPIE
   * Open a new terminal in ACPIE root directory
   * Run **prep_deps.sh** to install the required packages
   * Implement CRSF token functions in **acpied/data/user_fn.php** [Optional]
   * Generate MaciASL patches cache to enable MaciASL patches support [Optional]

## Generate MaciASL patches cache

To generate MaciASL patches cache, run the following command in ACPIE root directory:

`php make_urlpatches_cache.php`

## Sources

* [Cloud ACPI Editor](https://bitbucket.org/kylon/kylon/cloud-acpi-editor)
* [Jquery](https://github.com/jquery/jquery)
* [Boostrap](https://github.com/twbs/bootstrap)
* [Font Awesome](https://github.com/FortAwesome/Font-Awesome)
* [PsScrollbar Fork](https://bitbucket.org/kylon/perfect-scrollbar)
* [CodeMirror](https://github.com/codemirror/CodeMirror)
* [Guzzle](https://github.com/guzzle/guzzle)
* [SVG loaders](http://samherbert.net/svg-loaders/)

## Credits
* cpu icon by catkuro @[flaticon](https://www.flaticon.com)
* UI inspired by VS Code
* WebAssembly discord guys
