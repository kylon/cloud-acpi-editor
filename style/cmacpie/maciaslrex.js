// CodeMirror, copyright (c) by kylon and others
// Distributed under an MIT license: https://codemirror.net/LICENSE

(function(mod) {
    if (typeof exports == "object" && typeof module == "object") // CommonJS
        mod(require("codemirror-minified"));
    else if (typeof define == "function" && define.amd) // AMD
        define(["codemirror-minified"], mod);
    else // Plain browser env
        mod(CodeMirror);
})(function(CodeMirror) {
    "use strict";

    CodeMirror.defineMode("maciaslrex", function() {
        const extents = words([
           'into', 'into_all'
        ]);
        const scopes = words([
            'all','definitionblock','scope','method','device','processor','thermalzone',
        ]);
        const predicates = words([
            'label', 'name_adr', 'name_hid', 'code_regex', 'code_regex_not', 'parent_label', 'parent_type', 'parent_adr', 'parent_hid'
        ]);
        const actions = words([
            'insert','set_label','remove_entry','replace_content','replace_matched','replaceall_matched','remove_entry','remove_matched',
            'removeall_matched','store_%8','store_%9',
        ]);
        const codeDelimiter = words([
           'begin', 'end'
        ]);

        function tokenBase(stream, state) {
            const ch = stream.next();
            let word;

            switch (ch) {
                case "#": {
                    stream.skipToEnd();
                    return "comment";
                }
                default:
                    break;
            }

            stream.eatWhile(/[\w\$_]/);

            word = stream.current().toLowerCase();

            if (state.previousWord === 'code_regex' || state.previousWord === 'code_regex_not') {
                state.tokenize = tokenRegex();
                state.previousWord = '';

                return state.tokenize(stream, state);

            } else if (state.previousWord === 'begin') {
                state.codeBlock = true;

            } else if (word === 'end' && stream.peek() === ';') {
                state.codeBlock = false;
            }

            state.previousWord = word;

            if (state.codeBlock)
                return 'codeblock';
            else if (extents.hasOwnProperty(word))
                return 'extent';
            else if (scopes.hasOwnProperty(word))
                return 'scope';
            else if (predicates.hasOwnProperty(word))
                return 'predicate';
            else if (actions.hasOwnProperty(word))
                return 'action';
            else if (codeDelimiter.hasOwnProperty(word))
                return 'codedelim';

            return null;
        }

        function tokenRegex() {
            return function(stream, state) {
                let end = false;
                let next;

                while ((next = stream.next()) != null) {
                    if (next !== ' ')
                        continue;

                    end = true;
                    break;
                }

                if (end)
                    state.tokenize = null;

                return "regex";
            };
        }

        function words(array) {
            const keys = {};

            for (let i = 0, l = array.length; i < l; ++i)
                keys[array[i]] = true;

            return keys;
        }

        // Interface
        return {
            startState: function() {
                return {
                    tokenize: null,
                    previousWord: '',
                    codeBlock: false
                };
            },
            token: function(stream, state) {
                if (stream.eatSpace())
                    return null;

                return (state.tokenize || tokenBase)(stream, state);
            },

            electricChars: '{}',
            lineComment: '#'
        };
    });

    CodeMirror.defineMIME("text/x-maciaslrex", "maciaslrex");
});
