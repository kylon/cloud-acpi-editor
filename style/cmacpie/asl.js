// CodeMirror, copyright (c) by kylon and others
// Distributed under an MIT license: https://codemirror.net/LICENSE

(function(mod) {
    if (typeof exports == "object" && typeof module == "object") // CommonJS
        mod(require("codemirror-minified"));
    else if (typeof define == "function" && define.amd) // AMD
        define(["codemirror-minified"], mod);
    else // Plain browser env
        mod(CodeMirror);
})(function(CodeMirror) {
    "use strict";

    CodeMirror.defineMode("asl", function() {
        const keywords = words([
            'Default','DefinitionBlock','Device','Method','Else','ElseIf','For','Function','If','Include','Method','Return',
            'Scope','Switch','Case','While','Break','BreakPoint','Continue','NoOp','Wait','True','False','Name','OperationRegion',
            'Field','Offset','Package','Alias','AccessAs','Acquire','Buffer','ToPLD','CreateBitField','CreateByteField','CreateDWordField',
            'CreateField','CreateQWordField','CreateWordField','EisaId','IndexField','Release','Concatenate','ConcatenateResTemplate',
            'BankField','CondRefOf','Connection','CopyObject','DataTableRegion','Debug','ToBCD','ToBuffer','ToDecimalString','ToInteger',
            'ToString','ToUUID','Printf','Fprintf','Notify','FromBCD','Sleep','Processor','LoadTable','ThermalZone','Unicode',
            'Mutex','ObjectType','PowerResource','Mid','Fatal','FindSetLeftBit','FindSetRightBit','Event','Load','Unload','Match','Store',
            'Signal','SizeOf','Stall','StartDependentFn','StartDependentFnNoPri','Timer','Reset'
        ]);
        const strNumbers = words([
            'One', 'Ones', 'Zero'
        ]);
        const properties = words([
            'DMA','DWordIO','DWordMemory','DWordSpace','EndDependentFn','ExtendedIO',
            'ExtendedMemory','ExtendedSpace', 'FixedDMA','FixedIO','GpioInt','GpioIo','I2CSerialBusV2',
            'Interrupt','IO','IRQ','IRQNoFlags','Memory24','Memory32','Memory32Fixed',
            'QWordIO','QWordMemory','QWordSpace','RawDataBuffer','Register','ResourceTemplate',
            'SPISerialBusV2', 'UARTSerialBusV2','VendorLong','VendorShort','WordBusNumber','WordIO',
            'WordSpace'
        ]);
        const imports = words([
            'External'
        ]);
        const builtin = words([
            '__FILE__','__PATH__','__LINE__','__DATE__','__IASL__',
            '#define','#elif','#else','#endif','#error','#if','#ifdef','#ifndef','#include','#includebuffer',
            '#line','#pragma','#undef','#warning'
        ]);
        const atoms = words([
            'UnknownObj','IntObj','StrObj','BuffObj','PkgObj','FieldUnitObj','DeviceObj',
            'EventObj','MethodObj','MutexObj','OpRegionObj','PowerResObj','ProcessorObj',
            'ThermalZoneObj','BuffFieldObj','DDBHandleObj',
            'AttribQuick','AttribSendReceive','AttribByte','AttribBytes','AttribRawBytes',
            'AttribRawProcessBytes','AttribWord','AttribBlock','AttribProcessCall','AttribBlockProcessCall',
            'AnyAcc','ByteAcc','WordAcc','DWordAcc','QWordAcc','BufferAcc',
            'AddressRangeMemory','AddressRangeReserved','AddressRangeNVS','AddressRangeACPI',
            'RegionSpaceKeyword','FFixedHW','PCC',
            'AddressingMode7Bit','AddressingMode10Bit',
            'DataBitsFive','DataBitsSix','DataBitsSeven','DataBitsEight','DataBitsNine',
            'BusMaster','NotBusMaster',
            'ClockPhaseFirst','ClockPhaseSecond','ClockPolarityLow','ClockPolarityHigh',
            'SubDecode','PosDecode',
            'BigEndianing','LittleEndian',
            'FlowControlNone','FlowControlXon','FlowControlHardware',
            'Edge','Level','ActiveHigh','ActiveLow','ActiveBoth','Decode16','Decode10',
            'IoRestrictionNone','IoRestrictionInputOnly','IoRestrictionOutputOnly',
            'IoRestrictionNoneAndPreserve','Lock','NoLock','MTR','MEQ','MLE','MLT','MGE','MGT',
            'MaxFixed','MaxNotFixed','Cacheable','WriteCombining','Prefetchable','NonCacheable',
            'MinFixed','MinNotFixed',
            'ParityTypeNone','ParityTypeSpace','ParityTypeMark','ParityTypeOdd','ParityTypeEven',
            'PullDefault','PullUp','PullDown','PullNone','PolarityHigh','PolarityLow',
            'ISAOnlyRanges','NonISAOnlyRanges','EntireRange','ReadWrite','ReadOnly',
            'UserDefRegionSpace','SystemIO','SystemMemory','PCI_Config','EmbeddedControl',
            'SMBus','SystemCMOS','PciBarTarget','IPMI','GeneralPurposeIO','GenericSerialBus',
            'ResourceConsumer','ResourceProducer','Serialized','NotSerialized',
            'Shared','Exclusive','SharedAndWake','ExclusiveAndWake','ControllerInitiated','DeviceInitiated',
            'StopBitsZero','StopBitsOne','StopBitsOnePlusHalf','StopBitsTwo',
            'Width8Bit','Width16Bit','Width32Bit','Width64Bit','Width128Bit','Width256Bit',
            'SparseTranslation','DenseTranslation','TypeTranslation','TypeStatic',
            'Preserve','WriteAsOnes','WriteAsZeros','Transfer8','Transfer16','Transfer8_16',
            'ThreeWireMode','FourWireMode'
        ]);
        const operators = words([
            'Add','Decrement','Divide','Increment','Subtract','Index','LAnd','LEqual','LGreater','LGreaterEqual',
            'LLess','LLessEqual','LNot','LNotEqual','LOr','Mod','Multiply','NAnd','NOr','Not','Or','And','XOr',
            'RefOf','DerefOf','Revision','ShiftLeft','ShiftRight'
        ]);
        const isOperatorChar = /[!~\*/%+-<>\^|=&]/;
        const isVariable = /Arg[0-9]+$|Local[0-9]+$/;

        function tokenBase(stream, state) {
            const ch = stream.next();
            let word = null;

            switch (ch) {
                case "/": {
                    if (stream.eat("*")) {
                        state.tokenize = tokenComment;
                        return tokenComment(stream, state);

                    } else if (stream.eat("/")) {
                        stream.skipToEnd();
                        return "comment";
                    }
                }
                    break;
                case "[": {
                    state.tokenize = tokenFieldComment;
                    return tokenFieldComment(stream, state);
                }
                case '"': {
                    state.tokenize = tokenString(ch);
                    return state.tokenize(stream, state);
                }
                default:
                    break;
            }

            if (/[\(\),]/.test(ch)) {
                return null;

            } else if (/\d/.test(ch)) {
                stream.eatWhile(/[\w.]/);
                return "number";

            } else if (isOperatorChar.test(ch)) {
                stream.eatWhile(isOperatorChar);
                return "operator";
            }

            stream.eatWhile(/[\w\$_]/);

            word = stream.current();

            if (atoms.hasOwnProperty(word) || isVariable.test(word))
                return 'atom';
            else if (operators.hasOwnProperty(word))
                return 'operator';
            else if (strNumbers.hasOwnProperty(word))
                return 'number';
            else if (keywords.hasOwnProperty(word))
                return 'keyword';
            else if (properties.hasOwnProperty(word))
                return 'property';
            else if (imports.hasOwnProperty(word))
                return 'imports';
            else if (builtin.hasOwnProperty(word))
                return 'builtin';

            return null;
        }

        function tokenComment(stream, state) {
            let maybeEnd = false;
            let ch;

            while (ch = stream.next()) {
                if (ch === "/" && maybeEnd) {
                    state.tokenize = null;
                    break;
                }

                maybeEnd = (ch === "*");
            }

            return "comment";
        }

        function tokenFieldComment(stream, state) {
            let ch = stream.next();

            while (ch) {
                if ( ch === ']') {
                    state.tokenize = null;
                    break;
                }

                ch = stream.next();
            }

            return "comment";
        }

        function tokenString(quote) {
            return function(stream, state) {
                let escaped = false;
                let end = false;
                let next;

                while ((next = stream.next()) != null) {
                    if (next === quote && !escaped) {
                        end = true;
                        break;
                    }

                    escaped = !escaped && next === "\\";
                }

                if (end || !escaped)
                    state.tokenize = null;

                return "string";
            };
        }

        function words(array) {
            const keys = {};

            for (let i = 0, l = array.length; i < l; ++i)
                keys[array[i]] = true;

            return keys;
        }

        // Interface
        return {
            startState: function() {
                return {
                    tokenize: null
                };
            },
            token: function(stream, state) {
                if (stream.eatSpace())
                    return null;

                return (state.tokenize || tokenBase)(stream, state);
            },

            electricChars: '{}',
            lineComment: '//',
            blockCommentStart: '/*',
            blockCommentEnd: '*/',
            fold: 'brace'
        };
    });

    CodeMirror.defineMIME("text/x-asl", "asl");
});
