/*!
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * Licensed under GPLv3 license
 */
'use strict';

let autosaveInt = null;

onmessage = function(e) {
    switch (e.data[0]) { // cmd
        case 'start': {
            autosaveInt = this.setInterval(function() {
                postMessage(['atf']);
            }, 5*1000);
        }
            break;
        default:
            break;
    }
}