/*!
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * Licensed under GPLv3 license
 */
'use strict';

$(function() {
    const saveWorker = new Worker(Server.BasePath + '../style/js/workers/autosave.js');
    const acpiEditor = Utils.initAcpiEditor();
    const saveAsModalEl = $('#saveas');
    const settingsModalEl = $('#acpie-settings');
    const iaslCArgsTxEl = settingsModalEl.find('.iasl-uargs');
    const iaslDArgsTxEl = settingsModalEl.find('.iasl-udargs');
    const openFileInpt = $('.opentbin');
    const acpieInfoModal = $('#acpie-info');
    const iaslCheckAlert = Utils.patcherModal.find('.iaslrejcheck');
    const patcherCloseBtn = Utils.patcherModal.find('.btn-close');
    const patchApplyBtn = Utils.patcherModal.find('.patcher-apply-btn');
    const openPatchInpt = Utils.patcherModal.find('#opatchi');
    const patchesDeltaTable = Utils.patcherModal.find('.pdelta-tb');
    const patchesTotalCountEl = Utils.patcherModal.find('#pcnt');
    const patchesChangesCountEl = Utils.patcherModal.find('#pchgs');
    const iaslOutModal = $('#acpie-iasl-out');
    const acpiEditorElem = $('#wrapper .CodeMirror');
    const acpieState = {changed: false, busy: false};
    let compilerEvtDownloadTrigger = false;
    let patchesEditor = null;

    Utils.initAcpie();

    saveWorker.onmessage = e => { Utils.handleWorkerMsg(e, acpieState, acpiEditor); };
    saveWorker.postMessage(['start']);

    $(window).on('resize', function() {
        Scrollbars.updateAll();
    });

    // Bootstrap 5+ workaround
    $(document).on('click', '.form-control-label', function () {
        const _this = $(this);
        const input = _this.parent().find('input');

        $('.form-control-label[for="tempId"]').each(function () {
            $(this).attr('for', '');
        });

        $('.form-control-input[id="tempId"]').each(function () {
            $(this).attr('id', '');
        });

        if (!input.prop('disabled')) {
            _this.attr('for', 'tempId');
            input.attr('id', 'tempId');
        }
    });

    $(document).on('hidden.bs.toast', function () {
        Utils.showPendingNotification();
    });

    acpiEditor.on('change', function() {
        acpieState.changed = true;
    });

    Utils.patcherModal.on('shown.bs.modal', function () {
        if (patchesEditor == null)
            patchesEditor = Utils.initPatchesEditor();

        patchesEditor.focus();
        patchesEditor.setCursor({ line: 1 });
        Utils.toggleLoadingScrPatcher();
    });

    Utils.patcherModal.on('hide.bs.modal', function (e) {
        if (!patcherCloseBtn.hasClass('d-none'))
            return true;

        e.preventDefault();
        e.stopImmediatePropagation();
        return false;
    });

    patcherCloseBtn.on('click', function () {
        Utils.resetPatchEditor(patchesEditor, patchApplyBtn, patchesTotalCountEl, patchesChangesCountEl, patchesDeltaTable, iaslCheckAlert);
    });

    iaslOutModal.on('shown.bs.modal', function () {
        Scrollbars.IaslOutput.resetPosition();
        Scrollbars.IaslOutput.update();
    });

    $(document).on('compiler_evt', function (e, resData) {
        Utils.fetchReq(FetchURL.Editor, 'set_format_iasl_output', [resData], function (res) {
            if (res === false)
                throw new Error('no_iasl_o');

            if (compilerEvtDownloadTrigger) {
                if (resData.file !== '')
                    saveAsModalEl.find('.save-btn').parent().submit();

                saveAsModalEl.modal('hide');

                compilerEvtDownloadTrigger = false;
            }

            Utils.showIaslOutput(iaslOutModal, res);
            Utils.toggleLoadingScr();

        }).catch(e => {
            Utils.toggleLoadingScr();
            Utils.showNotification(ToastMessage.ENoIaslOutput);
            console.log(e);
        });
    });

    $(document).on('dcompiler_evt', function (e, resData) {
        if (resData.file === '') {
            Utils.showIaslOutput(iaslOutModal, resData.output);
            Utils.toggleLoadingScr();
            return false;
        }

        Utils.fetchReq(FetchURL.Editor, 'ses_new_table', null, (idx) => {
            if (idx === false)
                throw new Error('new_idx_e');

            Utils.fetchReq(FetchURL.Editor, 'set_acpi_table', [idx, resData.file, resData.name], (nidx) => {
                if (nidx === false)
                    throw new Error('set_table_e');

                Utils.addFileHandle(nidx);
                Utils.toggleLoadingScr();

            }).catch(e => {
                Utils.toggleLoadingScr();
                Utils.showNotification(ToastMessage.EOpenAcpiTable);
                console.log(e);
            });
        }).catch(e => {
            Utils.toggleLoadingScr();
            Utils.showNotification(ToastMessage.ECreateSessionForTable);
            console.log(e);
        });
    });

    $(document).on('register_wasm_iasl', async function (e, data) {
        await Utils.fetchReq(FetchURL.Editor, 'register_wasm_iasl', [data], res => {
            if (res === false)
                throw new Error('register_wasm_fail');

            Utils.initWasmIaslOpts(res);

        }).catch(e => {
            Utils.disableAmlSupp();
            Utils.showNotification(ToastMessage.ENoIaslInfo);
            console.log(e);
        });
    });

    $(document).on('click', '.tree-item', function () {
        const _this = $(this);
        const _line = parseInt(_this.parent().attr('data-line'), 10) - 1;

        $('.tree-item-active').removeClass('tree-item-active');
        _this.addClass('tree-item-active');
        acpiEditor.scrollIntoView({
            line: _line,
            char: 0
        }, 100);
        acpiEditor.focus();
        acpiEditor.setCursor({
           line: _line
        });
    });

    $(document).on('click', '.fold-icon', function (e) {
        e.stopPropagation();

        const _this = $(this);
        const elem = _this.parent().find('ol').first();
        const iconEl = _this.find('i');
        let arrowRm, arrowAdd;

        if (iconEl.hasClass('icon-caret-down')) {
            arrowRm = 'icon-caret-down';
            arrowAdd = 'icon-caret-right';

            elem.addClass('hidden');

        } else {
            arrowRm = 'icon-caret-right';
            arrowAdd = 'icon-caret-down';

            elem.removeClass('hidden');
        }

        iconEl.removeClass(arrowRm).addClass(arrowAdd);
    });

    /* iASL output */
    $(document).on('click', '[data-outtoline]', function () {
        const _line = parseInt($(this).attr('data-outtoline'), 10) - 1;

        if (_line < 0)
            return false;

        iaslOutModal.modal('hide');
        acpiEditor.scrollIntoView({
            line: _line,
            char: 0
        }, 100);
        acpiEditor.focus();
        acpiEditor.setCursor({
            line: _line
        });
    });

    openPatchInpt.on('change', async function () {
        const data = {
            'mpatches': this.files[0],
            'type': 'open_patches',
            'argC': 0
        }

        patchesEditor.setValue('');
        Utils.toggleLoadingScrPatcher();
        Utils.lockPatchModalOpenPatch(patchApplyBtn, patchesTotalCountEl, patchesChangesCountEl, patchesDeltaTable);

        await Utils.fetchReq(FetchURL.Editor, null, null, (res) => {
            if (res === false)
                throw new Error('open_ptch_e');

            patchesEditor.setValue(res);
            Utils.unlockPatchModalOpenPatch(iaslCheckAlert);
            Utils.toggleLoadingScrPatcher();

        }, 15, true, data).catch(e => {
            Utils.unlockPatchModalOpenPatch(iaslCheckAlert);
            Utils.toggleLoadingScrPatcher();
            Utils.showNotification(ToastMessage.EOpenPatchFile);
            console.log(e);
        });
    });

    openFileInpt.on('change', async function () {
        const formData = {
            'acpi-table': this.files[0],
            'type': 'open_table',
            'argC': 0
        }
        let data = null;

        Utils.toggleLoadingScr();

        await Utils.fetchReq(FetchURL.Editor, null, null, (res) => {
            data = res;
        }, 15, true, formData);

        if (data === false) {
            Utils.showNotification(ToastMessage.EOpenAcpiTable);
            Utils.toggleLoadingScr();
            return;
        }

        switch (data['success']) {
            case 'ok': {
                Utils.addFileHandle(data['idx']);
                Utils.toggleLoadingScr();
            }
                break;
            case 'no_native': {
                const ret = runIasl(data['bin'], data['args'], data['file'], 'd', data); // if ret==true wait for dcompiler_evt

                if (ret === false) {
                    Utils.showNotification(ToastMessage.EOpenAmlTable);
                    Utils.toggleLoadingScr();
                }
            }
                break;
            case 'no_aml': { // native iasl
                Utils.showIaslOutput(iaslOutModal, data['output']);
                Utils.toggleLoadingScr();
            }
                break;
            default: {
                Utils.showNotification(ToastMessage.EUnknown);
                Utils.toggleLoadingScr();
            }
                break;
        }
    });

    $(document).on('click', '.menu-btn', async function () {
        const _this = $(this);
        const type = _this.attr('data-type');

        switch (type) {
            case 'tree': {
                if (Utils.treeContainer.hasClass('acpitree-open')) {
                    Utils.treeContainer.removeClass('acpitree-open');
                    acpiEditorElem.removeClass('editor-tree-open');

                } else {
                    Utils.treeContainer.addClass('acpitree-open');
                    acpiEditorElem.addClass('editor-tree-open');
                }
            }
                break;
            case 'openf': {
                openFileInpt.trigger('click');
            }
                break;
            case 'patch': {
                Utils.toggleLoadingScrPatcher();
                Utils.patcherModal.modal('show');
            }
                break;
            case 'compile': {
                Utils.toggleLoadingScr();

                let iasl = null;

                await Utils.fetchReq(FetchURL.Editor, 'get_iasl_info', ['c'], function (res) {
                    iasl = res;

                    if (res === false)
                        throw new Error('iasl_no_info');

                }).catch(e => {
                    Utils.toggleLoadingScr();
                    Utils.showNotification(ToastMessage.ENoIaslInfo);
                    console.log(e);
                });

                if (iasl === false || iasl === null)
                    break;

                if (iasl['native'] === true) {
                    Utils.fetchReq(FetchURL.Editor, 'compile_dsl', null, function (res) {
                        if (res === false)
                            throw new Error('compile_fail');

                        Utils.showIaslOutput(iaslOutModal, res['output']);
                        Utils.toggleLoadingScr();

                    }).catch(e => {
                        Utils.toggleLoadingScr();
                        Utils.showNotification(ToastMessage.ECompileError);
                        console.log(e);
                    });

                } else {
                    const ret = runIasl(iasl['bin'], iasl['args'], acpiEditor.getValue(), 'c', null); // if ret==true wait for compiler_evt

                    if (ret === false) {
                        Utils.toggleLoadingScr();
                        Utils.showNotification(ToastMessage.ECompileError);
                    }
                }
            }
                break;
            case 'savedialog': {
                let signature = '';

                Utils.toggleLoadingScr();
                await Utils.fetchReq(FetchURL.Editor, 'get_signature', null, function (res) {
                    signature = typeof res === 'string' ? res:'';
                }).catch(() => {});

                $('input[name="filename"]').attr('placeholder', signature);
                Utils.toggleLoadingScr();
                saveAsModalEl.modal('show');
            }
                break;
            case 'info': {
                acpieInfoModal.modal('show');
            }
                break;
            case 'settings': {
                settingsModalEl.modal('show');
            }
                break;
            default:
                break;
        }
    });

    $(document).on('click', '[data-click]', async function (e) {
        const _this = $(this);
        const dataC = _this.attr('data-click');

        switch (dataC) {
            case 'add-file': {
                e.stopPropagation();

                await Utils.fetchReq(FetchURL.Editor, 'ses_new_table', null, (idx) => {
                    if (idx === false)
                        throw new Error('new_table_e');

                    Utils.addFileHandle(idx);

                }).catch((e) => {
                    Utils.showNotification(ToastMessage.ECreateSessionForTable);
                    console.log(e);
                });
            }
                break;
            case 'switch-table': {
                Utils.switchFile(_this, acpiEditor, acpieState);
            }
                break;
            case 'close-table': {
                e.stopPropagation();

                const el = _this.parent();
                const isActive = el.hasClass('fhandle-active');
                const listLen = el.parent().find('.file-handle').length;
                const idx = el.attr('data-idx');

                if (isActive && listLen > 1) {
                    let tmp = el.prev();

                    if (tmp.length === 0)
                        tmp = el.next();

                    await Utils.switchFile(tmp, acpiEditor, acpieState);
                    el.remove();
                    Scrollbars.Topbar.update();
                    Utils.deleteFile([idx, tmp.attr('data-idx')], null);

                } else if (listLen === 1) {
                    acpieState.busy = true;

                    Utils.toggleLoadingScr();
                    await Utils.deleteFile([idx, 'x'], res => { window.location.reload(); });

                } else {
                    Utils.deleteFile([idx, ''], res => {
                        el.remove();
                        Scrollbars.Topbar.update();
                    });
                }
            }
                break;
            case 'save-btn': {
                const extension = $('select[name="fextension"]').find(':selected').val();
                let compilerOut = null; // dsl
                let iasl = null;

                Utils.toggleLoadingScr();

                await Utils.fetchReq(FetchURL.Editor, 'get_iasl_info', ['c'], function (res) {
                    if (res === false)
                        throw new Error('iasl_no_info');

                    iasl = res;

                }).catch(e => {
                    Utils.toggleLoadingScr();
                    Utils.showNotification(ToastMessage.ENoIaslInfo);
                    console.log(e);
                });

                if (extension === 'aml' && iasl['native'] === false) {
                    const ret = runIasl(iasl['bin'], iasl['args'], acpiEditor.getValue(), 'c', null); // if ret==true wait for compiler_evt

                    if (ret !== false) {
                        compilerEvtDownloadTrigger = true;
                        break;
                    }

                    saveAsModalEl.modal('hide');
                    Utils.toggleLoadingScr();
                    Utils.showNotification(ToastMessage.ECompileError);
                    break;
                }

                if (extension === 'aml') {
                    await Utils.fetchReq(FetchURL.Editor, 'compile_dsl', null, (res) => {
                        compilerOut = res;

                        if (res === false)
                            throw new Error('compile_e');

                    }).catch(e => {
                        Utils.toggleLoadingScr();
                        Utils.showNotification(ToastMessage.ECompileError);
                        console.log(e);
                    });

                    if (compilerOut === false)
                        break;
                }

                if (extension !== 'aml' || compilerOut['success'] === true)
                    _this.parent().submit();

                saveAsModalEl.modal('hide');

                if (extension === 'aml')
                    Utils.showIaslOutput(iaslOutModal, compilerOut['output']);

                Utils.toggleLoadingScr();
            }
                break;
            case 'openupatch': {
                openPatchInpt.trigger('click');
            }
                break;
            case 'buildupatch': {
                const patches = patchesEditor.getValue().trim();
                let iaslCheck = null;

                if (patches === '')
                    break;

                Utils.lockPatchModalBuildPatch(patcherCloseBtn, patchApplyBtn, patchesDeltaTable, iaslCheckAlert);
                Utils.toggleLoadingScrPatcher();
                Utils.toggleLoadingScr();

                await Utils.fetchReq(FetchURL.Editor, 'check_iasl_ver_patch', [patches], (res) => {
                    iaslCheck = res;

                    if (res === false) {
                        Utils.showNotification(ToastMessage.EUnknown);
                        throw new Error('check_iasl_v_e');

                    } else if (res === -1) {
                        iaslCheckAlert.removeClass('d-none');
                        throw new Error('patch_iasl_ver_no_match');
                    }
                }).catch(e => {
                    Utils.unlockPatchModalBuildPatch(patcherCloseBtn);
                    Utils.toggleLoadingScrPatcher();
                    Utils.toggleLoadingScr();
                    console.log(e);
                });

                if (iaslCheck === -1 || iaslCheck === false)
                    break;

                Utils.fetchReq(FetchURL.Editor, 'build_patches', [patches], (res) => {
                    if (res === false)
                        throw new Error('no_build_p');

                    const hasDeltas = res[0] && res[0].length;
                    const totalC = res[1]['count'];
                    const changesC = hasDeltas ? res[1]['matches']:0;

                    if (hasDeltas)
                        Utils.showPatchesDelta(res[0], patchesDeltaTable);

                    patchesTotalCountEl.text(totalC);
                    patchesChangesCountEl.text(changesC);
                    Utils.unlockPatchModalBuildPatch(patcherCloseBtn);
                    Utils.toggleLoadingScrPatcher();
                    Utils.toggleLoadingScr();

                    if (hasDeltas)
                        patchApplyBtn.prop('disabled', false);

                }, 80).catch(e => {
                    Utils.unlockPatchModalBuildPatch(patcherCloseBtn);
                    Utils.toggleLoadingScrPatcher();
                    Utils.toggleLoadingScr();
                    Utils.showNotification(ToastMessage.EBuildPatches);
                    console.log(e);
                });
            }
                break;
            case 'applyupatch': {
                if (patchesEditor.getValue() === '')
                    return;

                acpieState.busy = true;

                Utils.lockPatchModalApplyPatch(patcherCloseBtn, patchApplyBtn);
                Utils.toggleLoadingScrPatcher();
                Utils.toggleLoadingScr();

                Utils.fetchReq(FetchURL.Editor, 'apply_patches', null, (res) => {
                    if (res === false)
                        throw new Error('no_apply');

                    acpiEditor.setValue(res);

                    Utils.resetPatchEditor(patchesEditor, patchApplyBtn, patchesTotalCountEl, patchesChangesCountEl, patchesDeltaTable, iaslCheckAlert);
                    Utils.unlockPatchModalApplyPatch(patcherCloseBtn);
                    Utils.toggleLoadingScrPatcher();
                    Utils.toggleLoadingScr();
                    Utils.saveGenTree(acpiEditor);

                    acpieState.busy = false;

                }).catch(e => {
                    Utils.unlockPatchModalApplyPatch(patcherCloseBtn);
                    Utils.toggleLoadingScrPatcher();
                    Utils.toggleLoadingScr();
                    Utils.showNotification(ToastMessage.EApplyPatches);
                    console.log(e);

                    acpieState.busy = false;
                });
            }
                break;
            default:
                break;
        }
    });

    $(document).on('change', '#patch-sel', async function () {
        const _this = $(this);
        const selected = _this.val();

        if (selected === '' || selected === undefined)
            return false;

        Utils.lockPatchModalGetPatchData(patchApplyBtn, patchesTotalCountEl, patchesChangesCountEl, patchesDeltaTable, iaslCheckAlert);
        Utils.toggleLoadingScrPatcher();

        await Utils.fetchReq(FetchURL.Editor, 'get_urlpatch_data', [selected], (res) => {
            if (res === false)
                throw new Error('no_patch_data');

            patchesEditor.setValue(res);
            Utils.unlockPatchModalGetPatchData();
            Utils.toggleLoadingScrPatcher();
            patchesEditor.focus();

        }).catch(e => {
            Utils.unlockPatchModalGetPatchData();
            Utils.toggleLoadingScrPatcher();
            Utils.showNotification(ToastMessage.ENoPatchData);
            console.log(e);
        });
    });

    $(document).on('change', '.iasl-rev, .diasl-rev', async function () {
        const _this = $(this);
        const el = _this.find('option:selected');
        const json = [_this.attr('name'), el.val()];

        await Utils.fetchReq(FetchURL.Editor, 'chg_sett', json, res => {
            if (res === false)
                throw new Error('chg_sett_e');

        }).catch(e => {
            Utils.showNotification(ToastMessage.ECompilerVersionSetting);
            console.log(e);
        });
    });

    $(document).on('change', '.iasl-cargs', async function () {
        Utils.writeiaslArg($(this), iaslCArgsTxEl);
        await Utils.fetchReq(FetchURL.Editor, 'chg_sett', ['iasl_cargs', iaslCArgsTxEl.val()], res => {
            if (res === false)
                throw new Error('chg_carg_e');

        }).catch(e => {
            Utils.showNotification(ToastMessage.ECompilerArgsSetting);
            console.log(e);
        });
    });

    $(document).on('change', '.iasl-dargs', async function () {
        Utils.writeiaslArg($(this), iaslDArgsTxEl);
        await Utils.fetchReq(FetchURL.Editor, 'chg_sett', ['iasl_dargs', iaslDArgsTxEl.val()], res => {
            if (res === false)
                throw new Error('chg_darg_e');

        }).catch(e => {
            Utils.showNotification(ToastMessage.EDisassemblerArgsSetting);
            console.log(e);
        });
    });
});