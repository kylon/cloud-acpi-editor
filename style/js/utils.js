/*!
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * Licensed under GPLv3 license
 */
'use strict';

const Server = (() => {
    function getACPIEBasePath() {
        const pathAr = window.location.pathname.split('/');
        let path = '';

        for (let i=0,l=pathAr.length; i<l; ++i) {
            path += pathAr[i] + '/';

            if (pathAr[i] === 'acpied')
                break;
        }

        return path;
    }

    const srv = {
        BasePath: getACPIEBasePath()
    };

    Object.freeze(srv);
    return srv;
})();

const FetchURL = (() => {
    const url = {
        Editor: Server.BasePath + 'data/xhr/write.php'
    };

    Object.freeze(url);
    return url;
})();

const ToastType = (() => {
    const type = {
        Success: 0,
        Error: 1
    };

    Object.freeze(type);
    return type;
})();

const ToastMessage = (() => {
    const msg = {
        // error
        EUnknown: 'Unknown error',
        ENoIaslOutput: 'Unable to obtain iasl output',
        EOpenAcpiTable: 'Could not open acpi table',
        EOpenAmlTable: 'Unable to disassemble the acpi table',
        ECloseFile: 'Failed to close file correctly',
        EChangeFile: 'Failed to switch to file',
        ENoPatchData: 'Unable to get patch data',
        EOpenPatchFile: 'Invalid MaciASL patch file',
        EBuildPatches: 'Failed to build patches',
        EApplyPatches: 'Failed to apply patches',
        ECreateSessionForTable: 'Could not create session file',
        ENoIaslInfo: 'Unable to gather wasm iasl information',
        ECompileError: 'Failed to build AML from ASL code',
        ECompilerVersionSetting: 'Unable to set compiler version',
        ECompilerArgsSetting: 'Unable to set compiler arguments',
        EDisassemblerArgsSetting: 'Unable to set disassembler arguments',
        EGenTableTree: 'Unable to generate table tree',

        // success
        Saving: 'Saving..'
    };

    Object.freeze(msg);
    return msg;
})();

const Utils = (() => {
    const _acpieTreeCont = $('#acpitree');
    const _settingsModal = $('#acpie-settings');
    const _patcherModal = $('#maciasl-patch');
    const _editorLoadingScr = $('#loadingscr');
    const _treeLoadingScr = _acpieTreeCont.find('#loadingtree');
    const _patchesEditorLoading = _patcherModal.find('#loading-peditor');
    const _patcherBuildLoadingScr = _patcherModal.find('.buildingp');
    const _patcherApplyLoadingScr = _patcherModal.find('.applyingp');
    const _patcherActionLoadingScr = _patcherModal.find('.patches-loadscr');
    const _patchBuildBtn = _patcherModal.find('.patcher-build-btn');
    const _patchOpenBtn = _patcherModal.find('.patcher-open-btn');
    const _patchSelectEl = _patcherModal.find('#patch-sel');
    const _iaslSettSelectEl = _settingsModal.find('select[name="iasl"]');
    const _iaslDisSettSelectEl = _settingsModal.find('select[name="iasl_dis"]');
    const _noAmlAlert = $('.no_aml');
    const _toastNotf = $('#acpietoast');
    const _fileList = $('#opened-list');
    const _notfQue = [];
    const util = {
        treeContainer: _acpieTreeCont,
        patcherModal: _patcherModal
    };

    function genTableTree(tree) {
        lockTree();

        if (tree) {
            _acpieTreeCont.find('.tree').html(tree);
            unlockTree();
            return;
        }

        util.fetchReq(FetchURL.Editor, 'gen_tree', null, (res) => {
            if (res === false)
                throw new Error('no_gen_tree');

            _acpieTreeCont.find('.tree').html(res);
            unlockTree();

        }).catch(e => {
            unlockTree();
            util.showNotification(ToastMessage.EGenTableTree);
            console.log(e);
        });
    }

    function lockTree() {
        _treeLoadingScr.removeClass('d-none');
        Scrollbars.AcpiTree.freeze();
    }

    function unlockTree() {
        _treeLoadingScr.addClass('d-none');
        Scrollbars.AcpiTree.unfreeze();
        Scrollbars.AcpiTree.update();
    }

    // https://github.com/kvz/locutus/blob/master/src/php/strings/nl2br.js
    function nl2br (str) {
        return str ? str.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br>$2') : '';
    }

    util.disableAmlSupp = () => {
        _noAmlAlert.removeClass('d-none');
        _iaslSettSelectEl.html('');
        _iaslDisSettSelectEl.html('');
        _settingsModal.find('select').prop('disabled', true);
        _settingsModal.find('textarea').prop('disabled', true);
        $('select[name="fextension"]').find('option[value="aml"]').remove();
        $('.menu-btn[data-type="compile"]').remove();
    }

    util.showPendingNotification = () => {
        const notf = _notfQue.shift();

        if (notf === undefined) {
            _toastNotf.parent().addClass('d-none');
            return;
        }

        util.showNotification(notf[1], notf[0]);
    }

    util.showNotification = (message, toastType = ToastType.Error) => {
        if (_toastNotf.hasClass('show') || _toastNotf.hasClass('shown')) {
            if (toastType === ToastType.Error)
                _notfQue.push([ToastType.Error, message]);
            else if (_notfQue.length > 0 && _notfQue[_notfQue.length - 1][0] !== ToastType.Success)
                _notfQue.push([ToastType.Success, message]);

            return;
        }

        const toastHeader = _toastNotf.find('.toast-header');

        if (toastType === ToastType.Success)
            toastHeader.removeClass('bg-danger').addClass('bg-success');
        else
            toastHeader.removeClass('bg-success').addClass('bg-danger');

        _toastNotf.find('.toast-body').text(message);
        _toastNotf.parent().removeClass('d-none');
        _toastNotf.toast('show');
    }

    util.addFileHandle = idx => {
        const fileHandle = _fileList.find('.file-handle').last();
        const node = fileHandle.clone();

        node.attr('title', idx).attr('data-idx', idx).removeClass('fhandle-active').find('.handle-name').text(idx.substr(0, idx.length-7));
        node.insertAfter(fileHandle);
        Scrollbars.Topbar.update();
    }

    util.deleteFile = async (json, callback=null) => {
        await util.fetchReq(FetchURL.Editor, 'del_ses_table', json, (res) => {
            if (res === false)
                throw new Error('delete_file_e');

            if (callback !== null)
                callback(res);

        }).catch(e => {
            util.showNotification(ToastMessage.ECloseFile);
            console.log(e);
        });
    }

    util.switchFile = async (obj, aEditor, edState) => {
        if (obj.hasClass('fhandle-active'))
            return;

        const json = [obj.attr('data-idx'), aEditor.getValue()];

        util.toggleLoadingScr();

        await util.fetchReq(FetchURL.Editor, 'switch_table', json, res => {
            if (res === false)
                throw new Error('switch_t_e');

            edState.busy = true;

            aEditor.setValue(res);
            aEditor.scrollIntoView({ line: 1, char: 0 });
            aEditor.focus();
            _fileList.find('.fhandle-active').removeClass('fhandle-active');
            obj.addClass('fhandle-active');
            util.toggleLoadingScr();
            genTableTree(null);

            edState.changed = false; // do not fire changed event of the editor
            edState.busy = false;

        }).catch(e => {
            util.toggleLoadingScr();
            util.showNotification(ToastMessage.EChangeFile);
            console.log(e);
        });
    }

    util.saveGenTree = aEditor => {
        lockTree();

        util.fetchReq(FetchURL.Editor, 'save_regen_tree', [aEditor.getValue(), ''], (res) => {
            if (res === false)
                throw new Error('svgen_e');

            util.showNotification(ToastMessage.Saving, ToastType.Success);
            genTableTree(typeof res === 'string' ? res:'');
            unlockTree();

        }).catch(e => {
            unlockTree();
            util.showNotification(ToastMessage.EUnknown);
            console.log(e);
        });
    }

    util.writeiaslArg = (_this, argTxEl) => {
        const argTxVal = argTxEl.val();
        const argTxCont = argTxVal.split(' ');
        const curV = _this.val();

        if (curV === 'del') {
            argTxEl.val('');

        } else {
            const space = argTxVal === '' ? '':' ';
            let exist = false;

            for (let i=0, len=argTxCont.length; i<len; ++i) {
                if (argTxCont[i] !== curV)
                    continue;

                exist = true;
                break;
            }

            if (!exist)
                argTxEl.val(argTxVal+space+curV);
        }
    }

    util.showIaslOutput = (outputElem, output) => {
        outputElem.find('.acpi-iasl-output').html(nl2br(output));//todo sanitize
        outputElem.modal('show');
    }

    util.showPatchesDelta = (delta, deltaTableEl) => {
        const deltaBodyEl = deltaTableEl.find('#pdelta-cont');
        const emptyN = deltaBodyEl.find('tr').first().clone();
        const frag = new DocumentFragment();

        deltaBodyEl.html('');
        frag.appendChild(emptyN.get(0));

        for (let i=0,l=delta.length; i<l; ++i) {
            const node = emptyN.clone();

            node.find('.bef').html((delta[i]['before']).replace(/(?:\\n|\n)/g, '<br>'));
            node.find('.aft').html((delta[i]['after']).replace(/(?:\\n|\n)/g, '<br>'));
            node.removeClass('d-none');

            frag.appendChild(node.get(0));
        }

        deltaBodyEl.append(frag);
        deltaTableEl.removeClass('d-none');
    }

    util.resetPatchEditor = (pEditor, applyBtn, totalPCountEl, changesCountEl, deltaTableEl, iaslChkAlert) => {
        pEditor.setValue('');
        applyBtn.prop('disabled', true);
        _patchBuildBtn.prop('disabled', false);
        totalPCountEl.text('0');
        changesCountEl.text('0');
        _patchSelectEl.prop('disabled', false).val('');
        deltaTableEl.addClass('d-none');
        iaslChkAlert.addClass('d-none');
    }

    util.fetchReq = async (url, type, vals, callback, tmout = 15, fileFetch = false, fdata = null) => {
        const init = () => {
            const head = {
                method: 'POST',
                cache: 'no-cache',
                headers: {
                    'tok': document.head.querySelector('meta[name="csrf-token"]').getAttribute('content')
                }
            };

            if (fileFetch) {
                const formData = new FormData();

                for (const key in fdata) {
                    if (!fdata.hasOwnProperty(key))
                        continue;

                    formData.append(key, fdata[key]);
                }

                head.body = formData;

            } else {
                head.headers['Content-Type'] = 'application/json';
                head.body = JSON.stringify({
                    type: type,
                    vals: vals
                });
            }

            return head;
        };

        const timeout = new Promise((resolve, reject) => {
            const tm = setTimeout(() => {
                clearTimeout(tm);
                reject(new Error('Connection timeout'));
            }, tmout * 1000);
        });

        const fetchP = fetch(url, init()).then(res => {
            if (res.ok && res.status === 200) {
                if (callback !== null)
                    return res.json().then(res => callback(res));
                else
                    return res.json().catch(e => {});
            }
        });

        return Promise.race([timeout, fetchP]);
    }

    util.initAcpie = () => {
        const _body = $('body');

        util.toggleLoadingScr();

        if (_body.hasClass('wasm')) {
            const binsNo = typeof iaslBins === 'undefined' ? 0 : Object.keys(iaslBins).length;

            _body.removeClass('wasm');

            if (binsNo <= 0) {
                util.disableAmlSupp();

            } else {
                const shared = {id: 0, end: binsNo, ret: [], erC: 0};
                let fail = 0;

                for (const iasl in iaslBins) {
                    const ret = runIasl(iasl, '', '', 'g', shared); // if ret==true wait for register_wasm_iasl

                    if (ret === false)
                        ++fail;
                }

                if (fail >= binsNo)
                    util.disableAmlSupp();
            }

        } else { // native iasl
            _iaslSettSelectEl.find('[value="x"]').remove();
            _iaslDisSettSelectEl.find('[value="x"]').remove();
        }

        util.toggleLoadingScr();
        genTableTree(null);
    }

    util.initWasmIaslOpts = iaslList => {
        if (iaslList.length === 0)
            return;

        const frag = new DocumentFragment();
        const frag2 = new DocumentFragment();
        let i = 0;

        for (const iasl of iaslList) {
            const node = document.createElement('option');

            node.value = (i++).toString(10);
            node.text = iasl.version;

            frag.appendChild(node);
            frag2.appendChild(node.cloneNode(true));
        }

        _iaslSettSelectEl.html('').append(frag);
        _iaslDisSettSelectEl.html('').append(frag2);
    }

    util.initAcpiEditor = () => {
        const editor = CodeMirror.fromTextArea(document.getElementById("editor"), {
            theme: "darcula",
            mode: "asl",
            lineNumbers: true,
            indentUnit: 4,
            undoDepth: 100,
            spellcheck: false,
            autocorrect: false,
            autocapitalize: false,
            autoCloseBrackets: true,
            matchBrackets: true,
            showTrailingSpace: true,
            styleActiveLine: true,
            scrollbarStyle: "overlay",
            dragDrop: false,
            extraKeys: {
                "Ctrl-F": "findPersistent",
                "Cmd-F": "findPersistent"
            }
        });

        editor.setSize('100%', '100%');
        editor.refresh();

        return editor;
    }

    util.initPatchesEditor = () => {
        const editor = CodeMirror.fromTextArea(document.getElementById("mpatch"), {
            theme: "darcula",
            mode: "maciaslrex",
            indentUnit: 2,
            undoDepth: 20,
            spellcheck: false,
            autocorrect: false,
            autocapitalize: false,
            autoCloseBrackets: true,
            matchBrackets: true,
            showTrailingSpace: true,
            styleActiveLine: true,
            scrollbarStyle: "overlay",
            dragDrop: false,
            extraKeys: {
                "Ctrl-F": "findPersistent",
                "Cmd-F": "findPersistent"
            }
        });

        editor.setSize('100%', '100%');
        editor.refresh();

        return editor;
    }

    util.handleWorkerMsg = (e, edState, aEditor) => {
        if (e.data[0] === 'atf' && edState.changed && !edState.busy) {
            edState.changed = false;
            util.saveGenTree(aEditor);
        }
    }

    util.toggleLoadingScr = () => {
        _editorLoadingScr.toggleClass('d-none');
    }

    util.toggleLoadingScrPatcher = () => {
        _patchesEditorLoading.toggleClass('d-none');
    }

    util.lockPatchModalGetPatchData = (applyBtn, totalPCountEl, changesCountEl, deltaTableEl, iaslChkAlert) => {
        applyBtn.prop('disabled', true);
        _patchBuildBtn.prop('disabled', false);
        totalPCountEl.text('0');
        changesCountEl.text('0');
        _patchSelectEl.prop('disabled', true);
        deltaTableEl.addClass('d-none');
        iaslChkAlert.addClass('d-none');
    }

    util.unlockPatchModalGetPatchData = () => {
        _patchSelectEl.prop('disabled', false);
    }

    util.lockPatchModalOpenPatch = (applyBtn, totalPCountEl, changesCountEl, deltaTableEl) => {
        applyBtn.prop('disabled', true);
        _patchOpenBtn.prop('disabled', true);
        _patchBuildBtn.prop('disabled', true);
        deltaTableEl.addClass('d-none');
        _patchSelectEl.prop('disabled', true).val('');
        totalPCountEl.text('0');
        changesCountEl.text('0');
    }

    util.unlockPatchModalOpenPatch = (iaslChkAlert) => {
        _patchBuildBtn.prop('disabled', false);
        _patchOpenBtn.prop('disabled', false);
        _patchSelectEl.prop('disabled', false);
        iaslChkAlert.addClass('d-none');
    }

    util.lockPatchModalBuildPatch = (closeBtn, applyBtn, deltaTableEl, iaslChkAlert) => {
        closeBtn.addClass('d-none');
        applyBtn.prop('disabled', true);
        deltaTableEl.addClass('d-none');
        _patcherBuildLoadingScr.removeClass('d-none');
        _patcherActionLoadingScr.removeClass('d-none');
        _patchBuildBtn.prop('disabled', true);
        _patchOpenBtn.prop('disabled', true);
        _patchSelectEl.prop('disabled', true);
        iaslChkAlert.addClass('d-none');
    }

    util.unlockPatchModalBuildPatch = (closeBtn) => {
        _patcherBuildLoadingScr.addClass('d-none');
        _patcherActionLoadingScr.addClass('d-none');
        _patchBuildBtn.prop('disabled', false);
        _patchOpenBtn.prop('disabled', false);
        _patchSelectEl.prop('disabled', false);
        closeBtn.removeClass('d-none');
    }

    util.lockPatchModalApplyPatch = (closeBtn, applyBtn) => {
        closeBtn.addClass('d-none');
        applyBtn.prop('disabled', true);
        _patcherApplyLoadingScr.removeClass('d-none');
        _patcherActionLoadingScr.removeClass('d-none');
        _patchBuildBtn.prop('disabled', true);
        _patchOpenBtn.prop('disabled', true);
        _patchSelectEl.prop('disabled', true);
    }

    util.unlockPatchModalApplyPatch = (closeBtn) => {
        _patcherApplyLoadingScr.addClass('d-none');
        _patcherActionLoadingScr.addClass('d-none');
        _patchBuildBtn.prop('disabled', false);
        _patchOpenBtn.prop('disabled', false);
        _patchSelectEl.prop('disabled', false);
        closeBtn.removeClass('d-none');
    }

    Object.freeze(util);
    return util;
})();

const Scrollbars = (() => {
    const obj = {
        Topbar: new PerfectScrollbar('#opened-list', {
            wheelPropagation: true,
            suppressScrollY: true,
            useBothWheelAxes: true
        }),
        IaslOutput: new PerfectScrollbar('.acpi-iasl-output', {
            wheelPropagation: true,
            wheelSpeed: 1.5
        }),
        Toolbar: new PerfectScrollbar('.toolbar', {
            wheelPropagation: true,
            suppressScrollX: true
        }),
        AcpiTree: new PerfectScrollbar('#acpitree', {
            wheelSpeed: 1.2
        })
    };

    obj.updateAll = () => {
        obj.Topbar.update();
        obj.IaslOutput.update();
        obj.Toolbar.update();
        obj.AcpiTree.update();
    }

    Object.freeze(obj);
    return obj;
})();