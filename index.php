<?php
/*
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * License - http://www.gnu.org/licenses/gpl-3.0.txt
 */
header("Content-Security-Policy: default-src 'self' data: ajax.cloudflare.com;");

if (version_compare(phpversion(), '7.3.0', '<')) {
    die('This PHP version is not supported, please upgrade!');
} else {
    header('Location: acpied/index.php', true, 301);
    exit();
}