<?php
/*
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * License - http://www.gnu.org/licenses/gpl-3.0.txt
 */

$link = [
    // CSS
    'psscrollbar' => '../style/css/perfect-scrollbar.min.css?20190717',
    'icomoon' => '../style/css/icomoon.min.css?20200730',
    'acpie' => '../style/css/acpied.css?20200730',
    'dark' => '../style/css/dark.css?20200730',
    'cmmaciasl-dark' => '../style/cmacpie/maciasl-theme-dark.css?20200714',

    // CSS - external
    'bootstrap' => '../node_modules/bootstrap/dist/css/bootstrap.min.css?500b2',
    'cm' => '../node_modules/codemirror-minified/lib/codemirror.css?5592',
    'cmsimplescrollbar' => '../node_modules/codemirror-minified/addon/scroll/simplescrollbars.css?5592',
    'cmdialog' => '../node_modules/codemirror-minified/addon/dialog/dialog.css?5592',

    // JS
    'psscrollbarjs' => '../style/js/libs/perfect-scrollbar.min.js?20190717',
    'cmasljs' => '../style/cmacpie/asl.js?5592',
    'cmmaciaslrexjs' => '../style/cmacpie/maciaslrex.js?5592',
    'wasmiasl' => 'iasl/wasm/wasm-iasl.js?20200824',
    'utilsjs' => '../style/js/utils.js?20200824',
    'editorjs' => '../style/js/editor.js?20200827',

    // JS - external
    'jquery' => '../node_modules/jquery/dist/jquery.slim.min.js?351',
    'bootstrapjs' => '../node_modules/bootstrap/dist/js/bootstrap.min.js?500b2',
    'cmjs' => '../node_modules/codemirror-minified/lib/codemirror.js?5592',
    'cmclosebrackets' => '../node_modules/codemirror-minified/addon/edit/closebrackets.js?5592',
    'cmmatchbrackets' => '../node_modules/codemirror-minified/addon/edit/matchbrackets.js?5592',
    'cmtrailspacejs' => '../node_modules/codemirror-minified/addon/edit/trailingspace.js?5592',
    'cmdialogjs' => '../node_modules/codemirror-minified/addon/dialog/dialog.js?5592',
    'cmsearchcursorjs' => '../node_modules/codemirror-minified/addon/search/searchcursor.js?5592',
    'cmsearchjs' => '../node_modules/codemirror-minified/addon/search/search.js?5592',
    'cmsimplescrollbarjs' => '../node_modules/codemirror-minified/addon/scroll/simplescrollbars.js?5592',
    'cmactivelinejs' => '../node_modules/codemirror-minified/addon/selection/active-line.js?5592'
];
