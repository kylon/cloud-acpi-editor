<?php
/*
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * License - http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * FOLLOW THIS SCHEME FOR TRANSLATIONS
 * 'DO NOT TRANSLATE' => 'TRANSLATE'
 * ANY INVALID TRANSLATION WILL NEVER BE APPROVED
 */

$text = [
    // DO NOT TRANSLATE
    'title' => 'Cloud ACPI Editor',
    'credit' => 'Cloud ACPI Editor'.' '.date('Y').' - kylon',
    'supported_ext' => '.dsl,.aml,.asl',

    // COMMON
    'diasl_ver' => 'Disassembler',
    'iasl_ver' => 'Assembler',
    'save' => 'Save',
    'before' => 'Before',
    'after' => 'After',
    'new' => 'New',
    'open' => 'Open',
    'patch' => 'Patch',
    'compile' => 'Compile',
    'download' => 'Download',
    'info' => 'Info',
    'settings' => 'Settings',
    'no_iasl_bins' => 'No iasl binary found! Support for AML files has been disabled',

    // TOOLBAR
    'acpi_tree' => 'ACPI Tree',

    // SETTINGS
    'glob_sett_t' => 'Global settings',
    'curr_sett_t' => 'Current file settings',
    'iasl_cargs_t' => 'Assembler arguments',
    'iasl_dargs_t' => 'Disassembler arguments',
    'iarg_del' => 'Clear arguments',

    // SAVE AS
    'save_title' => 'Save as',
    'dsl_text' => 'Disassembled',
    'aml_text' => 'Assembled',
    'file_extension' => 'File extension',

    // iASL OUTPUT MODAL
    'out_title' => 'Compiler Output',

    // INFO
    'info_useful_links' => 'Useful Links:',
    'author' => 'Author',

    // MACIASL PATCHES MODAL
    'maciasl_patch_t' => 'Patcher',
    'apply' => 'Apply',
    'build' => 'Build',
    'patches' => 'Patches',
    'changes' => 'Changes',
    'fetching_patches' => 'Fetching patches...',
    'rejected_iasl' => 'The patch has rejected the selected compiler version',
    'buildingp' => 'Building patches, this may take a few seconds...',
    'applyingp' => 'Applying patches, this may take a few seconds...',
];

$iaslCArgsTx = [
    '-vo' => 'Enable optimization comments [-vo]',
    '-va' => 'Disable all errors/warnings/remarks [-va]',
    '-ve' => 'Report only errors [-ve]',
    '-vi' => 'Less verbose errors and warnings [-vi]',
    '-vr' => 'Disable remarks [-vr]',
    '-we' => 'Report warnings as errors [-we]',
    '-oa' => 'Disable all optimizations [-oa]',
    '-of' => 'No constant folding [-of]',
    '-oi' => 'No int optimization to Zero/One/Ones [-oi]',
    '-on' => 'No named reference string optimization [-on]',
    '-ot' => 'No typechecking [-ot]',
    '-cr' => 'No Resource Descriptor error checking [-cr]',
    '-in' => 'Ignore NoOp operators [-in]',
];

$iaslDArgsTx = [
    '-db' => 'Do not translate Buffers to Resource Templates [-db]',
    '-dl' => 'Emit legacy ASL code only (no C-style operators) [-dl]',
    '-in' => 'Ignore NoOp operators [-in]',
];