<?php
/*
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * License - http://www.gnu.org/licenses/gpl-3.0.txt
 */
namespace ACPIE;

require_once __DIR__.'/../libs/vendor/autoload.php';
require_once __DIR__.'/../classes/acpieCache.php';

class maciASLURLPatchesParser { //todo user urls???
    /**
     * Flag to set/unset low RAM mode (some features will be disabled)
     *
     * @var bool
     */
    private $lowRAM;

    /**
     * Common patches URLs
     *
     * @var array
     */
    private $builtinURLs = [
        'ASRock' => 'http://maciasl.sourceforge.net/pjalm/asrock/',
        'ASUS' => 'http://maciasl.sourceforge.net/pjalm/asus/',
        'AsusSMC-Patches' => 'https://raw.githubusercontent.com/hieplpvip/AsusSMC/master/',
        'General' => 'http://maciasl.sourceforge.net/pjalm/general/',
        'Gigabyte' => 'http://maciasl.sourceforge.net/pjalm/gigabyte/',
        'Graphics' => 'http://maciasl.sourceforge.net/pjalm/graphics/',
        'Intel Series 6' => 'http://maciasl.sourceforge.net/pjalm/intel6/',
        'Intel Series 7' => 'http://maciasl.sourceforge.net/pjalm/intel7/',
        'Intel Series 8' => 'http://maciasl.sourceforge.net/pjalm/intel8/',
        'Intel Series 9' => 'http://maciasl.sourceforge.net/pjalm/intel9/',
        'MSI' => 'http://maciasl.sourceforge.net/pjalm/msi/',
        'kylon [Rehabman fork]' => 'https://raw.githubusercontent.com/kylon/Laptop-DSDT-Patch/master/',
        'MaciASL Sourceforge' => 'http://maciasl.sourceforge.net/',
        'Toleda HDMI' => 'https://raw.githubusercontent.com/toleda/audio_hdmi_uefi/master/',
        'Toleda HDMI 8' => 'https://raw.githubusercontent.com/toleda/audio_hdmi_8series/master/',
        'Toleda ALC' => 'https://raw.githubusercontent.com/toleda/audio_ALCInjection/master/',
        'VoodooI2C-Patches' => 'https://raw.githubusercontent.com/alexandred/VoodooI2C-Patches/master/',
        'Zotac' => 'http://maciasl.sourceforge.net/pjalm/zotac/'
    ];

    /**
     * Flag stating if patches from builin URLs have been loaded
     *
     * @var bool
     */
    private $initBuiltin = false;

    /**
     * Patches data
     *
     * @var array
     */
    private $patches = [];

    /**
     * List of all patches
     *
     * @var array
     */
    private $patchesList = [];

    /**
     * Temp variable used to insert patch data into the patches array
     *
     * @var string
     */
    private $currentFetchKey = '';

    public function __construct($lowram = true) {
        $this->lowRAM = $lowram;
    }

    /**
     * Extract patch names and file paths from .maciasl and enqueue a get request
     * to get patch data
     *
     * @param string $manifest
     * @param string $baseURL
     * @param string $patchNamesKey
     *
     * @return \Generator
     */
    private function getPatchesListFromManifest($manifest, $baseURL, $patchNamesKey) {
        $tmp = explode("\n", $manifest);

        foreach ($tmp as $line) {
            $tl = trim($line);

            if ($tl === '')
                continue;

            $t = array_values(array_filter(explode("\t", $tl)));

            if (!isset($t[1]))
                continue;

            yield $t[0] => new \GuzzleHttp\Psr7\Request('GET', $baseURL.(isset($t[2]) ? $t[2]:$t[1]));
        }
    }

    /**
     * Extract path data from raw URLs
     *
     * @param string $url
     * @param \GuzzleHttp\Client $httpClient
     * @param string $patchNamesKey
     */
    private function extractPatchesPlain($url, $httpClient, $patchNamesKey) {
        try {
            $resp = $httpClient->get($url.'.maciasl');
            if ($resp->getReasonPhrase() !== 'OK' || $resp->getStatusCode() !== 200)
                return;

        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return;
        }

        $pool = new \GuzzleHttp\Pool($httpClient, $this->getPatchesListFromManifest($resp->getBody()->getContents(), $url, $patchNamesKey), [
            'concurrency' => 6,
            'fulfilled' => function (\GuzzleHttp\Psr7\Response $resp, $name) {
                $this->patches[$this->currentFetchKey][$name] = $resp->getBody()->getContents();
            },
            'rejected' => function (\GuzzleHttp\Exception\RequestException $resp, $i) {},
        ]);

        $promise = $pool->promise();

        $promise->wait();
    }

    /**
     * Populate $patchesList
     */
    private function genPatchesList() {
        $this->patchesList = [];

        foreach ($this->patches as $label => $plist) {
            array_push($this->patchesList, $label);

            foreach ($plist as $pname => $data)
                array_push($this->patchesList, "$label;;$pname");
        }
    }

    /**
     * Return the list of extracted patches
     *
     * @return array
     */
    public function getPatchesList() {
        return $this->patchesList;
    }

    /**
     * Workaround method for servers with not enough RAM
     *
     * Return the list of extracted patches
     *
     * @return array
     */
    public function getPatchesListFromCache() {
        return acpieCache::getCachedMaciASLURLParserPatchesList();
    }

    /**
     * Return raw patches data
     *
     * @return array
     */
    public function getPatchesData() {
        return $this->patches;
    }

    /**
     * Import patches from external source
     * maciASLURLPatchesParser structure is required
     *
     * @param array $patchesList
     * @param array $patchesData
     */
    public function setPatchesListAndDataFrom($patchesList, $patchesData) {
        $this->patchesList = $patchesList;
        $this->patches = $patchesData;
    }

    /**
     * Get patch data
     *
     * @param string $id - format [label;;name]
     *
     * @return bool|string
     */
    public function getPatchData($id) {
        $t = explode(';;', $id);

        return isset($this->patches[$t[0]][$t[1]]) ? $this->patches[$t[0]][$t[1]]:false;
    }

    /**
     * Workaround method for servers with not enough RAM
     *
     * @param string $id
     *
     * @return bool|string
     */
    public function getPatchDataFromCache($id) {
        $patches = acpieCache::getCachedMaciASLURLParserPatchesData();
        $t = explode(';;', $id);

        return isset($patches[$t[0]][$t[1]]) ? $patches[$t[0]][$t[1]]:false;
    }

    /**
     * Extract patches from a list of URLs
     *
     * @param array $urls
     */
    public function extractPatchesFromURLs($urls) {
        if ($this->lowRAM)
            return;

        $httpClient = new \GuzzleHttp\Client([
                       'timeout' => 40.0,
                       'connect_timeout' => 30,
                       'allow_redirects' => false
                   ]);

        foreach ($urls as $name => $u) {
            if ($u[-1] !== '/')
                $u .= '/';

            $re = preg_match('/^https?:\/\//m', $u);

            if ($re === false || $re === 0)
                continue;

            $label = '.:: '.$name.' ::.';
            $this->patches[$label] = [];
            $this->currentFetchKey = $label;

            $this->extractPatchesPlain($u, $httpClient, $label);

            if (empty($this->patches[$label]))
                unset($this->patches[$label]);
            else
                ksort($this->patches[$label]);
        }

        $this->currentFetchKey = '';

        $this->genPatchesList();
        unset($httpClient);
    }

    /**
     * Extract patches from builtin URLs
     */
    public function extractFromBuiltins() {
        if ($this->initBuiltin || $this->lowRAM)
            return;

        $this->extractPatchesFromURLs($this->builtinURLs);
        $this->initBuiltin = true;
    }
}