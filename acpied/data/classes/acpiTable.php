<?php
/*
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * License - http://www.gnu.org/licenses/gpl-3.0.txt
 */
declare(strict_types = 1);

namespace ACPIE;

require_once __DIR__.'/enums.php';

Class acpiTable {
    /**
     * Disassembled ACPI table
     *
     * @var string
     */
    private $acpi_table = '';

    /**
     * Table tree
     *
     * @var array
     */
    private $table_tree = [];

    /**
     * HTML table tree
     *
     * @var string
     */
    private $table_tree_html = '';

    /**
     * Compiled ACPI table
     *
     * @var string
     *
     * @NOTE: This is a temp variable
     */
    private $acpi_table_aml = '';

    /**
     * Signature of the table
     *
     * @var string
     */
    private $acpi_table_sign = "ACPI";

    /**
     * Assembler output (formatted)
     *
     * @var string
     */
    private $iasl_output = '';

    /**
     * Table tree flag to generate a new tree (true when the file has changed)
     *
     * @var bool
     */
    private $hasChanged = false;

    /**
     * Flag stating if tree generation is supported for this table
     *
     * @var bool
     */
    private $hasTree = false;

    /**
     * acpiTable constructor
     *
     * @param string
     */
    public function __construct(string $file='') {
        $this->setAcpiTable($file);
    }

    /**
     * Helper - parse acpi table, return when a bracket is found
     *
     * @param integer &$pos
     * @param string  &$tmp
     * @param integer &$lineN
     *
     * @return string
     */
    private function getNextBracket(int &$pos, string &$tmp, int &$lineN): string {
        $tmp = '';

        while (isset($this->acpi_table[$pos])) {
            switch ($this->acpi_table[$pos]) {
                case ' ':
                case "\t": {
                    ++$pos;
                    continue 2;
                }
                    break;
                case '/': {
                    switch ($this->acpi_table[$pos+1]) {
                        case '*': { // multi-line comment
                            $pos += 2;

                            while (isset($this->acpi_table[$pos]) && ($this->acpi_table[$pos] !== '*' || $this->acpi_table[$pos+1] !== '/')) {
                                if ($this->acpi_table[$pos] === "\n" || $this->acpi_table[$pos] === "\r\n")
                                    ++$lineN;

                                ++$pos;
                            }

                            $pos += 2;
                            continue 3;
                        }
                            break;
                        case '/': { // inline comment
                            $pos += 2;

                            while (isset($this->acpi_table[$pos]) && $this->acpi_table[$pos] !== "\n" && $this->acpi_table[$pos] !== "\r\n")
                                ++$pos;

                            ++$pos;
                            ++$lineN;
                            continue 3;
                        }
                            break;
                        default:
                            break;
                    }
                }
                    break;
                case '[': { // field comment
                    ++$pos;

                    while (isset($this->acpi_table[$pos]) && $this->acpi_table[$pos] !== ']') {
                        if ($this->acpi_table[$pos] === "\n" || $this->acpi_table[$pos] === "\r\n")
                            ++$lineN;

                        ++$pos;
                    }

                    ++$pos;
                    continue 2;
                }
                    break;
                case "\r\n":
                case "\n": {
                    ++$pos;
                    ++$lineN;
                    continue 2;
                }
                    break;
                default:
                    break;
            }

            $tmp .= $this->acpi_table[$pos];

            if ($this->acpi_table[$pos] === '{' || $this->acpi_table[$pos] === '}')
                return $this->acpi_table[$pos++];

            ++$pos;
        }

        return '';
    }

    /**
     * Helper - extract the object name and return object type
     *
     * @param string &$str
     * @param string &$name
     *
     * @return string
     */
    private function extractObjectName(string &$str, string &$name): string {
        $objT = '';
        $i = -1;

        while (isset($str[$i]) && $str[$i] !== '(')
            --$i;

        if (!isset($str[$i])) // no '(' here, add as empty
            return '';

        for ($h=$i-1, $j=0; isset($str[$h]) && $j<5; ++$j,--$h)
            $objT = "$str[$h]$objT";

        ++$i;
        while (isset($str[$i]) && $str[$i] !== ')' && $str[$i] !== ',')
            $name .= $str[$i++];

        return strtolower($objT);
    }

    /**
     * Helper: Create an ACPI scope node
     *
     * @param integer $line
     * @param integer $startOff
     * @param integer $endOff
     * @param integer $scopeStr
     * @param string  $name
     *
     * @return array
     */
    private function genNode(int $line, int $startOff, int $endOff, int $scopeStr, string $name): array {
        return [
            'line' => $line,
            'startOffset' => intval($startOff),
            'endOffset' => intval($endOff),
            'scope' => $scopeStr,
            'name' => $name,
            'parent' => null,
            'children' => []
        ];
    }

    /**
     * Generate the table tree
     */
    private function _genTree(): void {
        $lineN = $pos = $disc = 0;
        $root = null;
        $tmp = '';

        while (isset($this->acpi_table[$pos])) {
            $char = $this->getNextBracket($pos, $tmp, $lineN);

            switch ($char) {
                case '{': {
                    $name = '';
                    $objT = $this->extractObjectName($tmp, $name);

                    switch ($objT) {
                        case 'block':
                            $node = $this->genNode($lineN, $pos, 0, ACPIScope::definitionblock, $this->acpi_table_sign);
                            $this->table_tree = $node;
                            $root = &$this->table_tree;
                            continue 3;
                        case 'scope':
                            $objT = ACPIScope::scope;
                            break;
                        case 'evice':
                            $objT = ACPIScope::device;
                            break;
                        case 'ethod':
                            $objT = ACPIScope::method;
                            break;
                        case 'lzone':
                            $objT = ACPIScope::thermalzone;
                            break;
                        case 'essor':
                            $objT = ACPIScope::processor;
                            break;
                        default:
                            ++$disc;
                            continue 3;
                    }

                    $node = $this->genNode($lineN, $pos, 0, $objT, $name);
                    $node['parent'] = &$root;

                    $cnt = array_push($root['children'], $node);
                    $root = &$root['children'][$cnt-1];
                }
                    break;
                case '}': {
                    if ($disc > 0) {
                        --$disc;
                        continue 2;
                    }

                    switch ($root['scope']) {
                        case ACPIScope::definitionblock:
                        case ACPIScope::scope:
                        case ACPIScope::device:
                        case ACPIScope::method:
                        case ACPIScope::processor:
                        case ACPIScope::thermalzone: {
                            $root['endOffset'] = $pos-1;
                            $root = &$root['parent'];
                        }
                            break;
                        default:
                            break;
                    }
                }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Generate HTML version of the Tree
     *
     * @param array $node - Root node of the table tree
     */
    private function _genHTMLTree(array $node): void {
        $hasChildren = !empty($node['children']);
        $icon = 'file';
        $caret = '';
        $tag = '';

        switch ($node['scope']) {
            case ACPIScope::scope:
                $icon = 'folder3';
                break;
            case ACPIScope::device:
                $icon = 'drive';
                break;
            case ACPIScope::method:
                $icon = 'box1';
                break;
            case ACPIScope::thermalzone:
                $icon = 'atomic';
                break;
            case ACPIScope::processor:
                $icon = 'cpu';
                break;
            default:
                break;
        }

        if ($hasChildren)
            $caret = '<span class="fold-icon"><i class="fa icon-caret-down"></i></span>';

        $this->table_tree_html .= "<li data-line=\"{$node['line']}\">{$caret}
            <span class=\"tree-item\"><span class=\"icon\"><i class=\"fa icon-{$icon}\"></i></span>{$node['name']}</span>";

        if ($hasChildren) {
            $this->table_tree_html .= '<ol>';
            $tag = 'o';
        }

        foreach ($node['children'] as $child)
            $this->_genHTMLTree($child);

        if ($tag === 'o')
            $this->table_tree_html .= '</ol>';

        $this->table_tree_html .= '</li>';
    }

    /**
     * Set a new ACPI Table
     *
     * @param string $file
     */
    public function setAcpiTable(string $file): void {
        $this->acpi_table_sign = 'UNK';
        $this->acpi_table_aml = '';
        $this->iasl_output = '';
        $this->table_tree_html = '';
        $this->table_tree = [];
        $this->acpi_table = $file;
        $this->hasChanged = true;

        $signature = [];

        $re = preg_match('/definitionblock\s*\(\s*[\s\S]*\s*,\s*\"(\w+)\"/iU', $file, $signature);
        if ($re === false)
            return;

        if (!empty($signature)) {
            $this->acpi_table_sign = $signature[1];
            $this->hasTree = true;

        } else {
            $re = preg_match('/signature\s*:\s*\"([\s\S]+)\"/iU', $file, $signature);

            if ($re !== false && !empty($signature))
                $this->acpi_table_sign = $signature[1];
        }
    }

    /**
     * Set AML code for this table
     *
     * @param string
     * @param boolean
     */
    public function setAmlTable(string $aml, bool $clearOutput = true): void {
        $this->acpi_table_aml = $aml;

        if ($clearOutput)
            $this->iasl_output = '';
    }

    /**
     * Set and format iASL output
     *
     * @param string
     */
    public function setFormatAssemblerOutput(string $output): void {
        $output = str_replace(
            ['Remarks','Remark','Warnings','Warning','Errors','Error','Optimizations','Optimization','Optimize'],
            ['<i class="out-remark">Remarks</i>','<i class="out-remark">Remark</i>',
                '<i class="out-warn">Warnings</i>','<i class="out-warn">Warning</i>',
                '<i class="out-error">Errors</i>','<i class="out-error">Error</i>',
                '<i class="out-opt">Optimizations</i>','<i class="out-opt">Optimization</i>','<i class="out-opt">Optimize</i>'],
            $output);
        $replace = '<div data-outtoline="$3"><i class="fw-bold iout-erline">Line $3</i>$4<br/>$5</div>';

        $this->iasl_output = preg_replace('/((^\/[\w\.\/]+\s*)(\d+)(\b.*\b.*$)\n(.*$)\n\n)/m', $replace, $output);
    }

    /**
     * Return the ACPI Table
     *
     * @return string
     */
    public function getAcpiTable(): string {
        return $this->acpi_table;
    }

    /**
     * Return the table signature
     *
     * @return string
     */
    public function getSignature(): string {
        return $this->acpi_table_sign;
    }

    /**
     * Return iASL output
     *
     * @return string
     */
    public function getAssemblerOutput(): string {
        return $this->iasl_output;
    }

    /**
     * Return the AML code of this table
     *
     * @return string
     */
    public function getAmlTable(): string {
        return $this->acpi_table_aml;
    }

    public function hasTreeGenSupport(): bool {
        return $this->hasTree;
    }

    /**
     * Return the table tree
     *
     * @return array
     */
    public function getTableTree(): array {
        return $this->table_tree;
    }

    /**
     * Return the table tree as HTML
     */
    public function getHTMLTableTree(): string {
        return $this->table_tree_html;
    }

    /**
     * Generate the table tree
     */
    public function genTree(): void {
        if (!$this->hasTree || $this->acpi_table === '' || !$this->hasChanged)
            return;

        $this->table_tree = [];
        $this->hasChanged = false;

        $this->_genTree();
    }

    /**
     * Generate HTML version of the table tree
     */
    public function genHTMLTree(): void {
        if (!$this->hasTree || $this->acpi_table === '' || !$this->hasChanged)
            return;

        if (empty($this->table_tree) || $this->hasChanged)
            $this->genTree();

        $this->table_tree_html .= '<ol>';

        $this->_genHTMLTree($this->table_tree);

        $this->table_tree_html .= '</ol>';
    }
}