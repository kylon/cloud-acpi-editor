<?php
/**
 * Cloud Clover Editor
 * Copyright (C) kylon 2016-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace ACPIE;

class settingsContainer {
    /**
     * Settings list (per object)
     *
     * @var array
     *
     * @notes setting => default value
     */
    private $settingsList = [
        'iasl' => 0, // selected assembler in iaslContainer
        'iasl_cargs' => '' // assembler args
    ];

    /**
     * Settings container
     *
     * @var array
     */
    private $container = [];

    /**
     * settingsContainer constructor
     */
    public function __construct() {}

    /**
     * Initialize settings for a given object
     *
     * @param string
     */
    public function initSettings(string $index): void {
        foreach ($this->settingsList as $setting => $val)
            $this->container[$index][$setting] = $val;
    }

    /**
     * Get a setting
     *
     * @param string
     * @param string
     *
     * @return mixed
     */
    public function get(string $index, string $setting) {
        if (!array_key_exists($index, $this->container) || !array_key_exists($setting, $this->container[$index]))
            return '';

        return $this->container[$index][$setting];
    }

    /**
     * Set a setting
     *
     * @param string
     * @param string
     * @param string|int
     *
     * @notes You must use initSettings first, if $index does not exist
     */
    public function set(string $index, string $setting, $val): void {
        if (array_key_exists($index, $this->container) && array_key_exists($setting, $this->container[$index]))
            $this->container[$index][$setting] = $val;
    }

    /**
     * Remove all settings for a given object
     *
     * @param string
     */
    public function remove(string $index): void {
        if (!array_key_exists($index, $this->container))
            return;

        unset($this->container[$index]);
    }

    /**
     * Replace an index
     *
     * @param string
     * @param string
     *
     * @return bool
     */
    public function updateIndex(string $old, string $new): bool {
        if (!array_key_exists($old, $this->container))
            return false;

        $this->container[$new] = $this->container[$old];

        unset($this->container[$old]);
        return true;
    }

    /**
     * Clear the container
     */
    public function clear(): void {
        unset($this->container);
        $this->container = [];
    }
}