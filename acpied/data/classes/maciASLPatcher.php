<?php
/*
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * License - http://www.gnu.org/licenses/gpl-3.0.txt
 */
namespace ACPIE;

require_once 'enums.php';

abstract class TokenType {
    const none = -1;
    const extent = 0;
    const scope = 1;
    const predicate = 2;
    const action = 3;
}

abstract class PatchScope {
    const all = 0;
    const definitionblock = 1;
    const scope = 2;
    const method = 3;
    const device = 4;
    const processor = 5;
    const thermalzone = 6;
}

abstract class PatchMatch {
    const regex = 0;
    const regexNot = 1;
    const label = 2;
    const address = 3;
    const hid = 4;
    const parentAddress = 5;
    const parentHid = 6;
    const parentLabel = 7;
    const parentType = 8;
}

abstract class PatchAction {
    const insert = 0;
    const replaceContent = 1;
    const setLabel = 2;
    const replaceMatched = 3;
    const replaceallMatched = 4;
    const removeMatched = 5;
    const removeallMatched = 6;
    const removeEntry = 7;
    const storeEight = 8;
    const storeNine = 9;
}

abstract class BlockType {
    const none = -1;
    const invalid = 99;
    const head = 0;
    const content = 1;
    const full = 2;
}

class maciASLPatcher {
    /**
     * Regex to extract _HID value
     *
     * @var string $hidRegex
     */
    private $hidRegex = '/Name\s*\(\s*_HID\s*,\s*(?:EISAID\s*\()?\"(.*)\"\s*\)?\s*\)/i';

    /**
     * Regex to extract _ADR value
     *
     * @var string $adrRegex
     */
    private $adrRegex = '/Name\s*\(\s*_ADR\s*,\s*(.*)\s*\)/';

    /**
     * Store store_%8 return value
     *
     * @var string $eight
     */
    private $eight = '';

    /**
     * Store store_%9 return value
     *
     * @var string $nine
     */
    private $nine = '';

    /**
     * Array of parsed MaciASL patches
     *
     * @var array $patches
     */
    private $patches = [];

    /**
     * Patches data
     *
     * @var array $patchesDelta
     */
    private $patchesDelta = [];

    /**
     * Last patched ACPI table
     *
     * This is a temp variable that stores the currently loaded table once patched
     * You can get this table to overwrite the unpatched one in acpiTable objects
     *
     * @var acpiTable $patchedTable
     */
    private $patchedTable = null;

    public function __construct() {}

    /**
     * Parse previously loaded patches and discard invalid ones
     */
    private function parsePatches() {
        $parsed = [];

        foreach ($this->patches as $patch) {
            $prevTokenType = TokenType::none;
            $hasPredicate = false;
            $isValid = false;
            $pdata = [];
            $i = 0;

            while (isset($patch[$i])) {
                $isValid = false;
                $token = '';

                while (isset($patch[$i]) && $patch[$i] === ' ')
                    ++$i;

                while (isset($patch[$i]) && $patch[$i] !== ' ')
                    $token .= $patch[$i++];

                ++$i;

                $token = strtolower($token);

                switch ($token) {
                    case 'into':
                    case 'into_all': {
                        if ($prevTokenType !== TokenType::none)
                            break 2;

                        $pdata['all'] = $token === 'into_all';
                        $prevTokenType = TokenType::extent;
                        $isValid = true;
                    }
                        break;
                    case 'all':
                    case 'definitionblock':
                    case 'scope':
                    case 'method':
                    case 'device':
                    case 'processor':
                    case 'thermalzone': {
                        if ($prevTokenType !== TokenType::extent)
                            break 2;

                        switch ($token) {
                            case 'all':
                                $pdata['scope'] = PatchScope::all;
                                break;
                            case 'definitionblock':
                                $pdata['scope'] = PatchScope::definitionblock;
                                break;
                            case 'scope':
                                $pdata['scope'] = PatchScope::scope;
                                break;
                            case 'method':
                                $pdata['scope'] = PatchScope::method;
                                break;
                            case 'device':
                                $pdata['scope'] = PatchScope::device;
                                break;
                            case 'processor':
                                $pdata['scope'] = PatchScope::processor;
                                break;
                            case 'thermalzone':
                                $pdata['scope'] = PatchScope::thermalzone;
                                break;
                        }

                        $prevTokenType = TokenType::scope;
                        $isValid = true;
                    }
                        break;
                    case 'label':
                    case 'name_adr':
                    case 'name_hid':
                    case 'parent_label':
                    case 'parent_type':
                    case 'parent_adr':
                    case 'parent_hid': {
                        if ($pdata['scope'] === PatchScope::definitionblock)
                            break 2;
                    }
                    case 'code_regex':
                    case 'code_regex_not': {
                        if ($prevTokenType === TokenType::action)
                            break 2;

                        $predT = -1;
                        $cont = '';

                        while (isset($patch[$i]) && $patch[$i] === ' ')
                            ++$i;

                        while (isset($patch[$i]) && $patch[$i] !== ' ')
                            $cont .= $patch[$i++];

                        if (!$hasPredicate) {
                            $pdata['predicates'] = [];
                            $hasPredicate = true;
                        }

                        switch ($token) {
                            case 'label':
                                $predT = PatchMatch::label;
                                break;
                            case 'name_adr':
                                $predT = PatchMatch::address;
                                break;
                            case 'name_hid':
                                $predT = PatchMatch::hid;
                                break;
                            case 'parent_label':
                                $predT = PatchMatch::parentLabel;
                                break;
                            case 'parent_type':
                                $predT = PatchMatch::parentType;
                                break;
                            case 'parent_adr':
                                $predT = PatchMatch::parentAddress;
                                break;
                            case 'parent_hid':
                                $predT = PatchMatch::parentHid;
                                break;
                            case 'code_regex': {
                                $predT = PatchMatch::regex;
                                $cont = preg_replace('/(?<!\\\\)\//', '\\\\/', $cont);

                                if ($cont === null)
                                    break 3;
                            }
                                break;
                            case 'code_regex_not': {
                                $predT = PatchMatch::regexNot;
                                $cont = preg_replace('/(?<!\\\\)\//', '\\\\/', $cont);

                                if ($cont === null)
                                    break 3;
                            }
                                break;
                        }

                        array_push($pdata['predicates'], ['predicate' => $predT, 'predicateCont' => $cont]);

                        ++$i;
                        $prevTokenType = TokenType::predicate;
                        $isValid = true;
                    }
                        break;
                    case 'insert':
                    case 'set_label':
                    case 'replace_matched':
                    case 'replaceall_matched':
                    case 'replace_content': {
                        if ($prevTokenType !== TokenType::predicate)
                            break 2;

                        $match = [];
                        $re = preg_match('/\s+begin\s+([\s\S]*)\s+end/', $patch, $match);

                        if ($re === false || empty($match))
                            break 2;

                        switch ($token) {
                            case 'insert':
                                $pdata['action'] = PatchAction::insert;
                                break;
                            case 'set_label':
                                $pdata['action'] = PatchAction::setLabel;
                                break;
                            case 'replace_matched':
                                $pdata['action'] = PatchAction::replaceMatched;
                                $match[1] = stripslashes(str_replace(['\n','\t'], ['\\\\n', '\\\\t'], $match[1]));
                                break;
                            case 'replaceall_matched':
                                $pdata['action'] = PatchAction::replaceallMatched;
                                $match[1] = stripslashes(str_replace(['\n','\t'], ['\\\\n', '\\\\t'], $match[1]));
                                break;
                            case 'replace_content':
                                $pdata['action'] = PatchAction::replaceContent;
                                break;
                        }

                        $pdata['actionCont'] = $match[1];
                        $prevTokenType = TokenType::action;
                        $i += strlen($match[0]) + 1;
                        $isValid = true;
                    }
                        break;
                    case 'store_%8':
                    case 'store_%9': {
                        if ($prevTokenType !== TokenType::predicate)
                            break 2;

                        $hasRegex = false;

                        foreach ($pdata['predicates'] as $pred) {
                            if ($pred['predicate'] === PatchMatch::regex) {
                                $hasRegex = true;
                                break;
                            }
                        }

                        if (!$hasRegex)
                            break 2;

                        switch ($token) {
                            case 'store_%8':
                                $pdata['action'] = PatchAction::storeEight;
                                break;
                            case 'store_%9':
                                $pdata['action'] = PatchAction::storeNine;
                                break;
                        }

                        $prevTokenType = TokenType::action;
                        $isValid = true;
                    }
                        break;
                    case 'remove_entry':
                    case 'remove_matched':
                    case 'removeall_matched': {
                        if ($prevTokenType !== TokenType::predicate)
                            break 2;

                        switch ($token) {
                            case 'remove_entry':
                                $pdata['action'] = PatchAction::removeEntry;
                                break;
                            case 'remove_matched':
                                $pdata['action'] = PatchAction::removeMatched;
                                break;
                            case 'removeall_matched':
                                $pdata['action'] = PatchAction::removeallMatched;
                                break;
                        }

                        $prevTokenType = TokenType::action;
                        $isValid = true;
                    }
                        break;
                    default:
                        break;
                }
            }

            if ($isValid)
                array_push($parsed, $pdata);
        }

        $this->patches = $parsed;
    }

    /**
     * Build all the patches for the given table
     *
     * @param resource $table
     * @param array    $node
     * @param array    $patch
     * @param array    &$results
     * @param boolean  &$stop
     */
    private function walk($table, $node, $patch, &$results, &$stop) {
        $match = false;

        switch ($patch['scope']) {
            case PatchScope::all:
                $match = true;
                break;
            case PatchScope::definitionblock:
                $match = $node['scope'] === ACPIScope::definitionblock;
                break;
            case PatchScope::scope:
                $match = $node['scope'] === ACPIScope::scope;
                break;
            case PatchScope::method:
                $match = $node['scope'] === ACPIScope::method;
                break;
            case PatchScope::device:
                $match = $node['scope'] === ACPIScope::device;
                break;
            case PatchScope::processor:
                $match = $node['scope'] === ACPIScope::processor;
                break;
            case PatchScope::thermalzone:
                $match = $node['scope'] === ACPIScope::thermalzone;
                break;
            default:
                break;
        }

        if ($match) {
            $ret = $this->buildPatch($table, $node, $patch);

            if (!$patch['all'] && !empty($results)) {
                $stop = true;
                return;
            }

            if ($ret !== null) {
                if (isset($ret[0])) {
                    foreach ($ret as $r)
                        array_push($results, $r);

                } else {
                    array_push($results, $ret);
                }
            }
        }

        foreach ($node['children'] as $child) {
            $this->walk($table, $child, $patch, $results, $stop);

            if ($stop)
                return;
        }
    }

    /**
     * Build a patch
     *
     * @param resource $table
     * @param array    $node
     * @param array    $patch
     *
     * @return array|null
     */
    private function buildPatch($table, $node, $patch) {
        $blockT = BlockType::none;
        $startOff = 0;
        $block = '';
        $reg = '';
        $r = [];

        foreach ($patch['predicates'] as $pred) {
            switch ($pred['predicate']) {
                case PatchMatch::regex: {
                    $reg = $pred['predicateCont'];

                    if ($pred['predicateCont'] === '.' && $patch['action'] !== PatchAction::replaceMatched &&
                        $patch['action'] !== PatchAction::replaceallMatched && $patch['action'] !== PatchAction::removeMatched &&
                        $patch['action'] !== PatchAction::removeallMatched)
                        break;

                    //$block = $this->getCodeBlock($node, $table, $startOff);
                    $block = $this->getBlockContent($node, $table, $startOff);
                    $blockT = BlockType::content;
                    $match = [];

                    $ret = preg_match('/'.$pred['predicateCont'].'/', $block, $match);
                    if ($ret === false || empty($match))
                        return null;
                }
                    break;
                case PatchMatch::regexNot: {
                    //$block = $this->getCodeBlock($node, $table, $startOff);
                    $block = $this->getBlockContent($node, $table, $startOff);
                    $blockT = BlockType::content;
                    $match = [];

                    $ret = preg_match('/'.$pred['predicateCont'].'/', $block, $match);
                    if ($ret === false || !empty($match))
                        return null;
                }
                    break;
                case PatchMatch::label: {
                    if ($node['name'] !== $pred['predicateCont'])
                        return null;
                }
                    break;
                case PatchMatch::address:
                case PatchMatch::hid: {
                    $rex = $pred['predicate'] === PatchMatch::hid ? $this->hidRegex:$this->adrRegex;
                    $block = $this->getCodeBlock($node, $table, $startOff);
                    $blockT = BlockType::full;
                    $match = [];

                    $re = preg_match($rex, $block, $match);
                    if ($re === false || empty($match) || $match[1] !== $pred['predicateCont'])
                        return null;
                }
                    break;
                case PatchMatch::parentAddress:
                case PatchMatch::parentHid: {
                    $parent = $node['parent'];

                    if ($parent === null)
                        return null;

                    $rex = $pred['predicate'] === PatchMatch::parentAddress ? $this->adrRegex:$this->hidRegex;
                    $block = $this->getBlockContent($parent, $table, $startOff);
                    $blockT = BlockType::invalid;
                    $match = [];

                    $re = preg_match($rex, $block, $match);
                    if ($re === false || empty($match) || $match[1] !== $pred['predicateCont'])
                        return null;
                }
                    break;
                case PatchMatch::parentLabel: {
                    $parent = $node['parent'];

                    if ($parent === null || $parent['name'] !== $pred['predicateCont'])
                        return null;
                }
                    break;
                case PatchMatch::parentType: {
                    $parent = $node['parent'];

                    if ($parent === null || ACPIScope::getVariableName($parent['scope']) !== strtolower($pred['predicateCont']))
                        return null;
                }
                    break;
                default:
                    break;
            }
        }

        switch ($patch['action']) {
            case PatchAction::insert: {
                $r = ['before' => '', 'after' => $patch['actionCont'], 'start' => $node['endOffset']-1];
            }
                break;
            case PatchAction::replaceContent: {
                if ($blockT !== BlockType::content)
                    $block = $this->getBlockContent($node, $table, $startOff);

                $r = ['before' => $block, 'after' => $patch['actionCont'], 'start' => $startOff];
            }
                break;
            case PatchAction::setLabel: {
                $label = $this->getLabel($node, $table, $startOff);

                if ($label === null)
                    return null;

                $r = ['before' => $label, 'after' => $patch['actionCont'], 'start' => $startOff];
            }
                break;
            case PatchAction::replaceallMatched:
            case PatchAction::removeallMatched:
            case PatchAction::replaceMatched:
            case PatchAction::removeMatched: {
                $match = [];
                $i = 0;

                if ($blockT !== BlockType::content)
                    $block = $this->getBlockContent($node, $table, $startOff);

                $re = preg_match_all('/'.$reg.'/', $block, $match, PREG_OFFSET_CAPTURE);
                if ($re === false || empty($match))
                    return null;

                while (isset($match[0][$i])) {
                    $blockStartOff = $startOff + $match[0][$i][1];
                    $after = '';

                    if (!$this->isInNode($node, $blockStartOff, $blockStartOff + strlen($match[0][$i][0]))) {
                        ++$i;
                        continue;
                    }

                    if ($patch['action'] === PatchAction::replaceMatched || $patch['action'] === PatchAction::replaceallMatched)
                        $after = $this->replaceMatchPlaceholders($patch['actionCont'], $match, $i);

                    array_push($r, ['before' => $match[0][$i][0], 'after' => $after, 'start' => $blockStartOff]);

                    if ($patch['action'] === PatchAction::replaceMatched || $patch['action'] === PatchAction::removeMatched)
                        break;

                    ++$i;
                }

                if (empty($r))
                    return null;
            }
                break;
            case PatchAction::removeEntry: {
                if ($blockT !== BlockType::full)
                    $block = $this->getCodeBlock($node, $table, $startOff);

                $r = ['before' => $block, 'after' => '', 'start' => $startOff];
            }
                break;
            case PatchAction::storeEight:
            case PatchAction::storeNine: {
                if ($reg === '')
                    return null;

                $match = [];
                $re = preg_match('/(?<!\\\\)\([\s\S]+(?<!\\\\)\)/', $reg, $match); // req. a capture group

                if ($re === false || empty($match))
                    return null;

                if ($blockT !== BlockType::content)
                    $block = $this->getBlockContent($node, $table, $startOff);

                $re = preg_match($reg, $block, $match);
                if ($re === false)
                    return null;

                switch ($patch['action']) {
                    case PatchAction::storeEight: {
                        $this->eight = isset($match[1]) ? $match[1]:$match[0];
                    }
                        break;
                    case PatchAction::storeNine: {
                        $this->nine = isset($match[1]) ? $match[1]:$match[0];
                    }
                        break;
                }

                $r = null; // has no delta
            }
                break;
            default:
                break;
        }

        return $r;
    }

    /**
     * Apply built patches
     *
     * @param resource $table
     * @param array    $data
     */
    private function apply($table, $data) {
        $shifted = [];

        foreach ($data as $p) {
            $startOf = $p['start'];
            $tmp = $startOf;
            $i = 0;

            while (isset($shifted[$i])) {
                if ($startOf < $shifted[$i]['off'])
                    break;

                $tmp += $shifted[$i]['diff'];
                ++$i;
            }

            $startOf = $tmp;

            if ($p['before'] === '') {
                fseek($table, $startOf-1);
                $nline = fread($table, 1) === "}" ? "\n":'';

                $after = $nline.str_replace(['\n', '\t'], ["\n", "\t"], $p['after']);//todo check remove \n?
                $dif = strlen($p['after']);

                $this->injectDataToStream($table, $after, $dif, $startOf);

            } else if ($p['after'] === '') {
                $beforeLen = strlen($p['before']);
                $this->removeDataFromStream($table, $startOf, $beforeLen);
                $dif = -$beforeLen;

            } else {
                $after = str_replace(['\n', '\t'], ["\n", "\t"], $p['after']);
                $beforeLen = strlen($p['before']);
                $afterLen = strlen($p['after']);
                $dif = -($beforeLen - $afterLen);

                $this->replaceDataInStream($table, $after, $afterLen, $startOf, $beforeLen);
            }

            if ($i === 0)
                array_push($shifted, ['off' => $p['start'], 'diff' => $dif]);
            else
                array_splice($shifted, $i, 0, [0 => ['off' => $p['start'], 'diff' => $dif]]);
        }
    }

    /**
     * Inject a string without overwriting
     * Thanks Mark Baker @stackoverflow
     *
     * @param resource $table
     * @param string   $cont
     * @param integer  $contL
     * @param integer  $pos
     */
    private function injectDataToStream($table, $cont, $contL, $pos) {
        $tmp = fopen('php://temp','rw+');

        stream_copy_to_stream($table, $tmp, -1, $pos);
        fseek($table, $pos);
        fwrite($table, $cont, $contL);
        rewind($tmp);
        stream_copy_to_stream($tmp, $table);
        fclose($tmp);
    }

    /**
     * Remove a portion of data
     *
     * @param resource $table
     * @param integer  $pos
     * @param integer  $len
     */
    private function removeDataFromStream($table, $pos, $len) {
        $tmp = fopen('php://temp','rw+');

        stream_copy_to_stream($table, $tmp, -1, $pos+$len);
        ftruncate($table, $pos);
        rewind($tmp);
        stream_copy_to_stream($tmp, $table);
        fclose($tmp);
    }

    /**
     * Replace a portion of data
     *
     * @param resource $table
     * @param string   $cont
     * @param integer  $contL
     * @param integer  $pos
     * @param integer  $len
     */
    private function replaceDataInStream($table, $cont, $contL, $pos, $len) {
        $tmp = fopen('php://temp','rw+');

        stream_copy_to_stream($table, $tmp, -1, $pos+$len);
        rewind($tmp);
        ftruncate($table, $pos+1);
        fseek($table, -1, SEEK_END);
        fwrite($table, $cont, $contL);
        stream_copy_to_stream($tmp, $table);
        fclose($tmp);
    }

    /**
     * Replace MaciASL regex placeholders with patch datas
     *
     * @param string  $patchCont
     * @param array   $vals
     * @param integer $idx
     *
     * @return string|null
     */
    private function replaceMatchPlaceholders($patchCont, $vals, $idx) {
        $match = [];

        $re = preg_match_all('/\%(\d)/', $patchCont, $match);
        if ($re === false)
            return null;

        if (empty($match[0])) {
            $re = preg_match_all('/\$(\d)/', $patchCont, $match);

            if ($re === false)
                return null;
            else if (empty($match[0]))
                return $patchCont;
        }

        for ($i=0; isset($match[0][$i]); ++$i) {
            if ($match[0][$i] === '%8')
                $patchCont = str_replace('%8', $this->eight, $patchCont);
            else if ($match[0][$i] === '%9')
                $patchCont = str_replace('%9', $this->nine, $patchCont);
            else
                $patchCont = str_replace($match[0][$i], $vals[$match[1][$i]][$idx][0], $patchCont);
        }

        return $patchCont;
    }

    /**
     * Get the content of a code block
     *
     * @param array    $node
     * @param resource $table
     * @param integer  &$nstartOff
     *
     * @return false|string
     */
    private function getBlockContent($node, $table, &$nstartOff) {
        $nstartOff = $node['startOffset'] + 1;
        $len = $node['endOffset'] - $node['startOffset'] - 1;

        if ($len <= 0)
            return '';

        fseek($table, $nstartOff, SEEK_SET);

        return fread($table, $len);
    }

    /**
     * Get head of a code block
     *
     * @param array    $node
     * @param resource $table
     * @param integer  &$nstartOff
     *
     * @return string
     */
    private function getBlockHead($node, $table, &$nstartOff) {
        $scope = ACPIScope::getVariableName($node['scope']);
        $i = $node['startOffset'];
        $typeLen = strlen($scope);
        $checkStop = false;
        $t = '';
        $c = '';

        while ($i >= 0) {
            if ($c === '(')
                $checkStop = true;
            else if ($checkStop && substr_compare($t, $scope, 0, $typeLen, true) === 0)
                break;

            fseek($table, $i--);

            $c = fread($table, 1);
            $t = "$c$t";
        }

        $nstartOff = $i + 1;

        return $t;
    }

    /**
     * Get a full block of code
     *
     * @param array    $node
     * @param resource $table
     * @param integer  &$nstartOff
     *
     * @return string
     */
    private function getCodeBlock($node, $table, &$nstartOff) {
        $block = $this->getBlockContent($node, $table, $nstartOff);
        $head = $this->getBlockHead($node, $table, $nstartOff);

        return "$head$block}";
    }

    /**
     * Extract the label of an object
     *
     * @param array    $node
     * @param resource $table
     * @param integer  &$nstartOff
     *
     * @return string|null
     */
    private function getLabel($node, $table, &$nstartOff) {
        $scope = ACPIScope::getVariableName($node['scope']);
        $labelRex = "/(?:$scope)\s*\(\s*([^\s),]+)/i";
        $block = $this->getBlockHead($node, $table, $nstartOff);
        $match = [];

        $ret = preg_match($labelRex, $block, $match, PREG_OFFSET_CAPTURE);
        if ($ret === false || empty($match))
            return null;

        $nstartOff += $match[1][1];

        return $match[1][0];
    }

    /**
     * Check if a block is inside the node delimited by start and end offsets
     *
     * @param array   $node
     * @param integer $startOff
     * @param integer $endOff
     *
     * @return bool
     */
    private function isInNode($node, $startOff, $endOff) {
        foreach ($node['children'] as $child) {
            if ($startOff > $child['startOffset'] && $endOff < $child['endOffset'])
                return false;
        }

        return true;
    }

    /**
     * Get patches data
     *
     * @return array
     */
    public function getPatchesDelta() {
        return $this->patchesDelta;
    }

    /**
     * Return the total number of patches and
     * how many of them are applicable
     *
     * @return array
     */
    public function getPatcherStats() {
        return ['count' => count($this->patches), 'matches' => count($this->patchesDelta)];
    }

    /**
     * Get the last patched table
     *
     * @return acpiTable|null
     */
    public function getPatchedTable() {
        return $this->patchedTable;
    }

    /**
     * Check if a specific compiler build is required for the loaded patches
     *
     * @param string $patches
     * @param string $iaslBuild
     *
     * @return boolean|int
     */
    public function checkCompilerBuildForPatches($patches, $iaslBuild) {
        $match = [];

        preg_match('/#\s*iasl\s*:\s*check\s*\$build\s*=\s*(\d+)/i', $patches, $match);

        if (empty($match))
            return true;

        return strpos($iaslBuild, $match[1]) !== false ? true:-1;
    }

    /**
     * Try to load patches from a string of patches
     *
     * @param string $patches
     *
     * @return boolean
     */
    public function loadPatches($patches) {
        $tmp = array_filter(explode("\n", $patches));
        $lineC = count($tmp);
        $multiLine = false;
        $multiL = '';
        $lineN = 0;

        $this->patchedTable = null;
        $this->patches = [];
        $this->eight = '';
        $this->nine = '';

        if ($patches == '')
            return false;

        foreach ($tmp as $line) {
            $tmp2 = trim($line);
            $push = false;

            ++$lineN;

            if ($tmp2[0] === '#')
                continue;

            $scI = strpos($tmp2, ';');

            if ($lineN === $lineC && $scI === false) { // last line, patch end character is optional
                $line .= ';'; // add missing char
                $patchEnd = true;

            } else {
                $patchEnd = $scI !== false && $tmp2[$scI] === $tmp2[-1];
            }

            if ($multiLine) {
                $multiL .= ' '.$line;

                if ($patchEnd) {
                    $tmp2 = $multiL;
                    $multiLine = false;
                    $push = true;
                }

            } else if (!$patchEnd) {
                $multiLine = true;
                $multiL = $tmp2;

            } else { // single line patch
                $push = true;
            }

            if ($push) {
                $tmp2[-1] = ' ';
                $tmp2 = rtrim($tmp2).' ';

                array_push($this->patches, $tmp2);
            }
        }

        $this->parsePatches();
        return true;
    }

    /**
     * Build all the loaded patches for the given table
     *
     * @param acpiTable $tableObj - acpiTable object
     *
     * @return boolean
     */
    public function buildPatches($tableObj) {
        $this->patchesDelta = [];

        if (!$tableObj->hasTreeGenSupport() || empty($this->patches))
            return false;

        $stream = fopen('php://temp','rw+');
        $this->patchedTable = $tableObj;

        if ($stream === false)
            return false;

        $ret = fwrite($stream, $tableObj->getAcpiTable());
        if ($ret === false)
            return false;

        $ret = rewind($stream);
        if ($ret === false)
            return false;

        foreach ($this->patches as $patch) {
            $results = [];
            $stop = false;

            $this->walk($stream, $this->patchedTable->getTableTree(), $patch, $results, $stop);

            if (empty($results))
                continue;

            $this->patchesDelta = array_merge($this->patchesDelta, $results);

            $this->apply($stream, $results);
            $this->patchedTable->setAcpiTable(stream_get_contents($stream, -1, 0));
            $this->patchedTable->genTree();
        }

        fclose($stream);
        return true;
    }
}