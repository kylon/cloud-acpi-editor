<?php
/*
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * License - http://www.gnu.org/licenses/gpl-3.0.txt
 */
declare(strict_types = 1);

namespace ACPIE;

class acpieCache {
    /**
     * Cache path
     *
     * @var string
     */
    private static $path = __DIR__.'/../../cache';

    private function __construct() {}

    /**
     * Return the cached maciASLURLPatchesParser patches list
     *
     * @return array
     */
    public static function getCachedMaciASLURLParserPatchesList(): array {
        return require_once acpieCache::$path.'/maciASLPatchesListCache.php';
    }

    /**
     * Return the cached maciASLURLPatchesParser patches data
     *
     * @return array
     */
    public static function getCachedMaciASLURLParserPatchesData(): array {
        return require_once acpieCache::$path.'/maciASLPatchesDataCache.php';
    }
}