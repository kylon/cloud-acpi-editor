<?php
namespace ACPIE;

abstract class ACPIScope {
    const unk = -1;
    const definitionblock = 0;
    const scope = 1;
    const device = 2;
    const method = 3;
    const processor = 4;
    const thermalzone = 5;

    public static function getVariableName($value) {
        $class = new \ReflectionClass(__CLASS__);
        $constants = array_flip($class->getConstants());

        return $constants[$value];
    }
}