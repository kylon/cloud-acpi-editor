<?php
/**
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace ACPIE;

class iaslBinary {
    /**
     * Path to iasl binary
     *
     * @var string
     */
    private $path;

    /**
     * Version string
     *
     * @var string
     */
    private $version;

    /**
     * Flag stating if this is a native binary
     *
     * @var boolean
     */
    private $native;

    /**
     * iaslBinary constructor.
     *
     * @param string $path
     * @param string $vers
     * @param boolean $native [default false]
     */
    public function __construct(string $path, string $vers, bool $native = false) {
        $this->path = $path;
        $this->version = $vers;
        $this->native = $native;
    }

    /**
     * Return iasl binary path
     *
     * @return string
     */
    public function getPath(): string {
        return $this->path;
    }

    /**
     * Return iasl version
     *
     * @return string
     */
    public function getVersion(): string {
        return $this->version;
    }

    /**
     * Return a flag stating if this binary is native (true) or wasm (false)
     *
     * @return bool
     */
    public function isNative(): bool {
        return $this->native;
    }
}