<?php
/**
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace ACPIE;

class iaslContainer {
    /**
     * Container
     *
     * @var array
     */
    private $container = [];

    /**
     * Container size
     *
     * @var int
     */
    private $size = 0;

    /**
     * Active disassembler binary
     *
     * @var int
     */
    private $activeDisassemblerIdx = -1;

    /**
     * acpiContainer constructor.
     */
    public function __construct() {}

    /**
     * Return the iasl binary
     *
     * @param int $index
     *
     * @return iaslBinary|false
     */
    public function getBinary(int $index) {
        if (!isset($this->container[$index]))
            return false;

        return $this->container[$index];
    }

    /**
     * Add a binary to the container
     *
     * @param iaslBinary $iaslBin
     */
    public function addBinary(iaslBinary $iaslBin): void {
        array_push($this->container, $iaslBin);
        ++$this->size;
    }

    /**
     * Sort iasl binaries by file name
     *
     * @param bool $descending - default is ascending
     */
    public function sortBinariesByFileName(bool $descending = false): void {
        usort($this->container, function ($a, $b) {
            return strcasecmp($a->getPath(), $b->getPath());
        });

        if ($descending)
            $this->container = array_reverse($this->container);
    }

    /**
     * Return the size of the container
     *
     * @return int
     */
    public function getSize(): int {
        return $this->size;
    }

    /**
     * Return the list of available iasl binaries
     *
     * @return array
     */
    public function getBinariesList(): array {
        $re = [];

        foreach ($this->container as $iasl)
            array_push($re, ['bin' => $iasl->getPath(), 'version' => $iasl->getVersion()]);

        return $re;
    }

    /**
     * Set the active disassembler binary
     *
     * @param int $index
     */
    public function setActiveDisassembler(int $index): void {
        $this->activeDisassemblerIdx = $index;
    }

    /**
     * Return the active disassembler index
     *
     * @return int
     */
    public function getActiveDisassemblerIdx(): int {
        return $this->activeDisassemblerIdx;
    }

    /**
     * Return the active disassembler
     *
     * @return iaslBinary|false
     */
    public function getDisassembler() {
        if (!isset($this->container[$this->activeDisassemblerIdx]))
            return false;

        return $this->container[$this->activeDisassemblerIdx];
    }
}