<?php
/**
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
declare(strict_types = 1);

namespace ACPIE;

class acpiContainer {
    /**
     * Container
     *
     * @var array
     */
    private $container = [];

    /**
     * Active file
     *
     * @var string
     */
    private $activeIdx = '';

    /**
     * acpiContainer constructor.
     */
    public function __construct() {}

    /**
     * Generate a unique index for a file
     *
     * @param string
     *
     * @return string
     */
    private function genIndex(string $fileName): string {
        return $fileName.'-'.substr(sha1(strval(time())), rand(0, 33), 6);
    }

    /**
     * Get a file from the container
     * If no index is specified, returns the active file
     *
     * Returns an empty string if no file is found
     *
     * @param string
     *
     * @return acpiTable|null
     */
    public function getAcpiTableObj(string $index=''): ?acpiTable {
        if ($index === '' && array_key_exists($this->activeIdx, $this->container))
            return $this->container[$this->activeIdx];
        else if ($index !== '' && array_key_exists($index, $this->container))
            return $this->container[$index];

        return null;
    }

    /**
     * Get a list of all the file indexes in the container
     *
     * @return array
     */
    public function getIndexesList(): array {
        return array_keys($this->container);
    }

    /**
     * Get container size
     */
    public function getSize(): int {
        return count($this->container);
    }

    /**
     * Get active file index
     *
     * @return string
     */
    public function getActiveIdx(): string {
        return $this->activeIdx;
    }

    /**
     * Set the active file
     *
     * @param string
     */
    public function setActiveIdx($index): void {
        $this->activeIdx = $index;
    }

    /**
     * Add a new file to the container
     *
     * @param acpiTable
     * @param string
     * @param bool
     */
    public function add(acpiTable $obj, string $name = '', bool $setActive=false): void {
        $idxName = $name !== '' ? $name:$obj->getSignature();
        $index = $this->genIndex($idxName);

        while (array_key_exists($index, $this->container))
            $index = $this->genIndex($idxName);

        $this->container[$index] = $obj;

        if ($setActive)
            $this->activeIdx = $index;
    }

    /**
     * Remove a file from the container
     *
     * @param string
     */
    public function remove(string $index): void {
        if (!array_key_exists($index, $this->container))
            return;
        else if ($index === $this->activeIdx)
            $this->activeIdx = '';

        unset($this->container[$index]);
    }

    /**
     * Update a file
     *
     * @param string
     * @param acpiTable
     */
    public function update(string $index, acpiTable $obj): void {
        if (array_key_exists($index, $this->container))
            $this->container[$index] = $obj;
    }

    /**
     * Set a new index for an existing table in the container
     *
     * @param string $oldIdx
     * @param string $newFileName
     *
     * @return bool|string
     */
    public function updateIndex(string $oldIdx, string $newFileName) {
        if (!isset($this->container[$oldIdx]))
            return false;

        $newIdx = $this->genIndex($newFileName);
        $this->container[$newIdx] = $this->container[$oldIdx];

        unset($this->container[$oldIdx]);
        return $newIdx;
    }

    /**
     * Clear the container
     */
    public function clear(): void {
        unset($this->container);
        $this->container = [];
    }
}