<?php
/*
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * License - http://www.gnu.org/licenses/gpl-3.0.txt
 */
session_start();

require_once __DIR__.'/../classes/acpiTable.php';
require_once __DIR__.'/../classes/acpiContainer.php';
require_once __DIR__.'/../classes/iaslContainer.php';

if (!isset($_SESSION['acpi-list']) || !isset($_SESSION['iasl-list']) || !filter_has_var(INPUT_POST, 'filename'))
    exit(1);

$iaslList = unserialize($_SESSION['iasl-list']);
$acpiList = unserialize($_SESSION['acpi-list']);
$table = $acpiList->getAcpiTableObj('');
$fname = filter_input(INPUT_POST, 'filename', FILTER_SANITIZE_STRING);
$fileName = $fname != '' ? $fname:$table->getSignature();
$extension = '.'.filter_input(INPUT_POST, 'fextension');
$mime = 'text/x-asl';

if ($extension === '.aml') {
    if ($iaslList->getSize() === 0)
        exit(1);

    $file = $table->getAmlTable();
    $mime = 'application/octet-stream';

    $table->setAmlTable('');

} else {
    $file = $table->getAcpiTable();
}

if (gettype($file) !== 'string') {
    $fileName = 'ErrorOccurred';
    $file = '';
}

header("Cache-Control: no-cache, must-revalidate");
header("Content-Type: ".$mime);
header('Content-Disposition: attachment; filename="'.$fileName.$extension.'"');
header('Content-Transfer-Encoding: binary');

echo $file;
exit(0);