<?php
/*
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * License - http://www.gnu.org/licenses/gpl-3.0.txt
 */
session_start();

require_once __DIR__.'/../user_fn.php';
require_once __DIR__.'/../utils.php';

if (!isValidToken() || !isset($_SESSION['acpi-list']) || !isset($_SESSION['sett-list']) || !isset($_SESSION['miaslUrlParser']) || !isset($_SESSION['maciaslPatcher']) || !isset($_SESSION['iasl-list']))
    goto exit_error;

$fetchData = json_decode(file_get_contents('php://input'), true);
$hasPost = filter_has_var(INPUT_POST, 'type');

if (($fetchData === null && !$hasPost) || ($hasPost && !filter_has_var(INPUT_POST, 'argC')))
    goto exit_error;

$type = $hasPost ? filter_input(INPUT_POST, 'type', FILTER_SANITIZE_STRING):filter_var($fetchData['type'], FILTER_SANITIZE_STRING);
$argC = isset($fetchData['vals']) && $fetchData['vals'] !== null ? count($fetchData['vals']):0;
$re = false;

switch ($type) {
    case 'open_table': {
        if (!isset($_FILES['acpi-table']) || $_FILES['acpi-table']['tmp_name'] === '' ||
                filter_input(INPUT_POST, 'argC', FILTER_SANITIZE_NUMBER_INT) !== '0')
            break;

        $iaslList = unserialize($_SESSION['iasl-list']);
        $acpiList = unserialize($_SESSION['acpi-list']);
        $settList = unserialize($_SESSION['sett-list']);
        $file = file_get_contents($_FILES['acpi-table']['tmp_name']);
        $finfo = new finfo(FILEINFO_MIME);
        $isAML = substr_compare($finfo->file($_FILES['acpi-table']['tmp_name']), 'text', 0, 4) !== 0;
        $name = substr($_FILES['acpi-table']['name'], -4, 1) === '.' ? substr($_FILES['acpi-table']['name'], 0, -4):$_FILES['acpi-table']['name'];
        $ret = ['success' => '', 'args' => '', 'bin' => '', 'output' => '', 'file' => '', 'name' => '', 'idx' => ''];

        if ($isAML) {
            if ($iaslList->getSize() === 0 || !isset($_SESSION['iasl_dargs']))
                break;

            $iaslBin = $iaslList->getDisassembler();
            if ($iaslBin === false)
                break;

            $iasl = $iaslBin->getPath();
            $args = $_SESSION['iasl_dargs'];

            if (!$iaslBin->isNative()) { // js will handle this
                $ret['success'] = 'no_native';
                $ret['args'] = $args;
                $ret['bin'] = $iasl;
                $ret['file'] = stringToByteArray($file);
                $ret['name'] = $name;

                $re = $ret;
                break;
            }

            $tempOutput = tmpfile();
            $metaDataOutput = stream_get_meta_data($tempOutput);
            $tmpFilenameOutput = $metaDataOutput['uri'];
            $out = $_FILES['acpi-table']['tmp_name'].'.dsl';

            exec("{$iasl} {$args} {$_FILES['acpi-table']['tmp_name']} &> {$tmpFilenameOutput}");

            if (file_exists($out)) {
                $file = file_get_contents($out);

                unlink($out);
                fclose($tempOutput);

            } else {
                $ret['success'] = 'no_aml';
                $ret['output'] = str_replace("\n", '<br/>', file_get_contents($tmpFilenameOutput));

                $re = $ret;

                fclose($tempOutput);
                break;
            }
        }

        $table = new \ACPIE\acpiTable($file);

        $acpiList->add($table, $name);

        $idxList = $acpiList->getIndexesList();

        if (count($idxList) === 0)
            break;

        $newIdx = end($idxList);

        $settList->initSettings($newIdx);

        $_SESSION['acpi-list'] = serialize($acpiList);
        $_SESSION['sett-list'] = serialize($settList);

        $ret['success'] = 'ok';
        $ret['file'] = $file;
        $ret['idx'] = $newIdx;

        $re = $ret;
    }
        break;
    case 'ses_new_table': {
        $acpiList = unserialize($_SESSION['acpi-list']);
        $settList = unserialize($_SESSION['sett-list']);
        $name = isset($fetchData['vals'][0]) ? $fetchData['vals'][0]:'';
        $table = new \ACPIE\acpiTable();

        $acpiList->add($table);

        $idxList = $acpiList->getIndexesList();

        if (count($idxList) === 0)
            break;

        $newIdx = end($idxList);

        $settList->initSettings($newIdx);

        $_SESSION['acpi-list'] = serialize($acpiList);
        $_SESSION['sett-list'] = serialize($settList);
        $re = $newIdx;
    }
        break;
    case 'del_ses_table': {
        if ($argC != 2)
            break;

        if ($fetchData['vals'][1] == 'x') { // no more files, reset acpie
            unset($_SESSION);
            session_destroy();

        } else {
            $acpiList = unserialize($_SESSION['acpi-list']);
            $settList = unserialize($_SESSION['sett-list']);
            $isActive = $acpiList->getActiveIdx() === $fetchData['vals'][0];

            $acpiList->remove($fetchData['vals'][0]);
            $settList->remove($fetchData['vals'][0]);

            if ($isActive)
                $acpiList->setActiveIdx($fetchData['vals'][1]);

            $_SESSION['acpi-list'] = serialize($acpiList);
            $_SESSION['sett-list'] = serialize($settList);
        }

        $re = true;
    }
        break;
    case 'register_wasm_iasl': {
        if ($argC !== 1)
            break;

        $iaslList = unserialize($_SESSION['iasl-list']);

        foreach ($fetchData['vals'][0] as $iasl) {
            $bin = new \ACPIE\iaslBinary($iasl['bin'], getWasmBinaryVersion($iasl['output']));

            $iaslList->addBinary($bin);
        }

        $iaslList->sortBinariesByFileName();

        if ($iaslList->getSize() > 0) {
            $acpiList = unserialize($_SESSION['acpi-list']);
            $settList = unserialize($_SESSION['sett-list']);

            $settList->set($acpiList->getActiveIdx(), 'iasl', 0);
            $iaslList->setActiveDisassembler(0);

            $_SESSION['sett-list'] = serialize($settList);
            $_SESSION['acpi-list'] = serialize($acpiList);
        }

        $_SESSION['iasl-list'] = serialize($iaslList);
        $_SESSION['try_wasm'] = '';
        $re = $iaslList->getBinariesList();
    }
        break;
    case 'set_acpi_table': {
        if ($argC > 3)
            break;

        $acpiList = unserialize($_SESSION['acpi-list']);
        $settList = unserialize($_SESSION['sett-list']);
        $table = $acpiList->getAcpiTableObj($fetchData['vals'][0]);
        $ret = true;

        $table->setAcpiTable(byteArrayToString($fetchData['vals'][1]));
        $acpiList->update($fetchData['vals'][0], $table);

        if (isset($fetchData['vals'][2])) {
            $newIdx = $acpiList->updateIndex($fetchData['vals'][0], $fetchData['vals'][2]);

            if ($newIdx === false)
                break;

            $settList->updateIndex($fetchData['vals'][0], $newIdx);

            $ret = $newIdx;
        }

        $_SESSION['acpi-list'] = serialize($acpiList);
        $_SESSION['sett-list'] = serialize($settList);
        $re = $ret;
    }
        break;
    case 'chg_sett': {
        if ($argC !== 2)
            break;

        switch ($fetchData['vals'][0]) {
            case 'iasl_cargs': {
                $acpiList = unserialize($_SESSION['acpi-list']);
                $settList = unserialize($_SESSION['sett-list']);

                $settList->set($acpiList->getActiveIdx(), $fetchData['vals'][0], filterSupportedIaslArgs($fetchData['vals'][1], 'c'));

                $_SESSION['sett-list'] = serialize($settList);
                $re = true;
            }
                break;
            case 'iasl_dargs': {
                $_SESSION['iasl_dargs'] = filterSupportedIaslArgs($fetchData['vals'][1], 'd');
                $re = true;
            }
                break;
            case 'iasl': {
                $acpiList = unserialize($_SESSION['acpi-list']);
                $settList = unserialize($_SESSION['sett-list']);

                $settList->set($acpiList->getActiveIdx(), $fetchData['vals'][0], intval($fetchData['vals'][1]));

                $_SESSION['sett-list'] = serialize($settList);
                $re = true;
            }
                break;
            case 'iasl_dis': {
                $iaslList = unserialize($_SESSION['iasl-list']);

                $iaslList->setActiveDisassembler(intval($fetchData['vals'][1]));

                $_SESSION['iasl-list'] = serialize($iaslList);
                $re = true;
            }
                break;
            default:
                break;
        }
    }
        break;
    case 'get_signature': {
        $idx = $argC === 1 ? $fetchData['vals'][0]:''; // '' = active idx
        $acpiList = unserialize($_SESSION['acpi-list']);
        $table = $acpiList->getAcpiTableObj($idx);
        $re = $table->getSignature();
    }
        break;
    case 'set_format_iasl_output': {
        if ($argC !== 1)
            break;

        $acpiList = unserialize($_SESSION['acpi-list']);
        $table = $acpiList->getAcpiTableObj('');
        $file = $fetchData['vals'][0]['file'];

        if (is_array($file))
            $file = byteArrayToString($file);

        $table->setFormatAssemblerOutput($fetchData['vals'][0]['output']);
        $table->setAmlTable($file, false);

        $_SESSION['acpi-list'] = serialize($acpiList);
        $re = $table->getAssemblerOutput();
    }
        break;
    case 'gen_tree': {
        $acpiList = unserialize($_SESSION['acpi-list']);
        $table = $acpiList->getAcpiTableObj('');

        $table->genHTMLTree();
        $acpiList->update($acpiList->getActiveIdx(), $table);

        $_SESSION['acpi-list'] = serialize($acpiList);
        $re = $table->getHTMLTableTree();
    }
        break;
    case 'save_regen_tree': { // save and get the new tree
        if ($argC != 2)
            break;

        $acpiList = unserialize($_SESSION['acpi-list']);
        $table = $acpiList->getAcpiTableObj($fetchData['vals'][1]);

        if ($table === '')
            break;

        $table->setAcpiTable($fetchData['vals'][0]);
        $table->genHTMLTree();
        $acpiList->update($fetchData['vals'][1], $table);

        $_SESSION['acpi-list'] = serialize($acpiList);
        $re = $table->getHTMLTableTree();
    }
        break;
    case 'switch_table': {
        if ($argC != 2)
            break;

        $acpiList = unserialize($_SESSION['acpi-list']);
        $current = $acpiList->getAcpiTableObj('');
        $next = $acpiList->getAcpiTableObj($fetchData['vals'][0]);

        $current->setAcpiTable($fetchData['vals'][1]); // Update current table content
        $acpiList->update($acpiList->getActiveIdx(), $current); // Update container
        $acpiList->update($fetchData['vals'][0], $next); // Update container
        $acpiList->setActiveIdx($fetchData['vals'][0]); // set next as active

        $_SESSION['acpi-list'] = serialize($acpiList);
        $re = $next->getAcpiTable();
    }
        break;
    case 'get_iasl_info': {
        if ($argC !== 1)
            break;

        $iaslList = unserialize($_SESSION['iasl-list']);

        if ($fetchData['vals'][0] === 'c') {
            $acpiList = unserialize($_SESSION['acpi-list']);
            $settList = unserialize($_SESSION['sett-list']);
            $idx = $acpiList->getActiveIdx();
            $iasl = $iaslList->getBinary($settList->get($idx, 'iasl'));

            if ($iasl === false)
                break;

            $re = ['native' => $iasl->isNative(), 'bin' => $iasl->getPath(), 'args' => $settList->get($idx, 'iasl_cargs')];

        } else if ($fetchData['vals'][0] === 'd') {
            $iasl = $iaslList->getDisassembler();

            if ($iasl === false)
                break;

            $re = ['native' => $iasl->isNative(), 'bin' => $iasl->getPath(), 'args' => $_SESSION['iasl_dargs']];
        }
    }
        break;
    case 'compile_dsl': {
        $iaslList = unserialize($_SESSION['iasl-list']);

        if ($iaslList->getSize() === 0)
            break;

        $acpiList = unserialize($_SESSION['acpi-list']);
        $settList = unserialize($_SESSION['sett-list']);
        $idx = $acpiList->getActiveIdx();
        $table = $acpiList->getAcpiTableObj($idx);
        $args = $settList->get($idx, 'iasl_cargs');
        $iaslBin = $iaslList->getBinary($settList->get($idx, 'iasl'));
        $success = false;

        if ($iaslBin === false || !$iaslBin->isNative())
            break;

        $iasl = $iaslBin->getPath();
        $fileCont = $table->getAcpiTable();
        $tempDSL = tmpfile();
        $tempOutput = tmpfile();

        fwrite($tempDSL, $fileCont);

        $metaDataDSL = stream_get_meta_data($tempDSL);
        $metaDataOutput = stream_get_meta_data($tempOutput);

        $tmpFilenameDSL = $metaDataDSL['uri'];
        $tmpFilenameOutput = $metaDataOutput['uri'];

        exec("{$iasl} {$args} {$tmpFilenameDSL} &> {$tmpFilenameOutput}");
        fclose($tempDSL);

        $table->setFormatAssemblerOutput(file_get_contents($tmpFilenameOutput));
        fclose($tempOutput);

        if (file_exists($tmpFilenameDSL.'.aml')) {
            $file = file_get_contents($tmpFilenameDSL.'.aml');
            $success = true;

            $table->setAmlTable($file, false);
            unlink($tmpFilenameDSL.'.aml');
        }

        $acpiList->update($idx, $table);

        $_SESSION['acpi-list'] = serialize($acpiList);
        $re = ['output' => $table->getAssemblerOutput(), 'success' => $success];
    }
        break;
    case 'get_urlpatch_data': {
        if ($argC !== 1)
            break;

        $parser = unserialize($_SESSION['miaslUrlParser']);

        $re = $parser->getPatchDataFromCache($fetchData['vals'][0]);
    }
        break;
    case 'check_iasl_ver_patch': {
        if ($argC !== 1)
            break;

        $iaslList = unserialize($_SESSION['iasl-list']);
        $patcher = unserialize($_SESSION['maciaslPatcher']);
        $acpiList = unserialize($_SESSION['acpi-list']);
        $settList = unserialize($_SESSION['sett-list']);
        $iaslIdx = $settList->get($acpiList->getActiveIdx(), 'iasl');
        $iasl = $iaslList->getBinary($iaslIdx);

        if ($iasl === false)
            break;

        $re = $patcher->checkCompilerBuildForPatches($fetchData['vals'][0], $iasl->getVersion());
    }
        break;
    case 'open_patches': {
        if (!isset($_FILES['mpatches']) || $_FILES['mpatches']['tmp_name'] === '' || $_FILES['mpatches']['type'] !== 'text/plain' ||
                filter_input(INPUT_POST, 'argC', FILTER_SANITIZE_NUMBER_INT) !== '0')
            break;

        $pfile = file_get_contents($_FILES['mpatches']['tmp_name']);

        if ($pfile === false || $pfile === '' || mb_detect_encoding($pfile, 'UTF-8', true) === false)
            break;

        $re = $pfile;
    }
        break;
    case 'build_patches': {
        if ($argC !== 1)
            break;

        $patches = trim($fetchData['vals'][0]);
        if ($patches === '') {
            $re = true; // no error, just empty str
            break;
        }

        $acpiList = unserialize($_SESSION['acpi-list']);
        $patcher = unserialize($_SESSION['maciaslPatcher']);

        $ret = $patcher->loadPatches($patches);
        if ($ret === false)
            break;

        $ret = $patcher->buildPatches($acpiList->getAcpiTableObj(''));
        if ($ret === false)
            break;

        $_SESSION['maciaslPatcher'] = serialize($patcher);
        $re = [$patcher->getPatchesDelta(), $patcher->getPatcherStats()];
    }
        break;
    case 'apply_patches': {
        $acpiList = unserialize($_SESSION['acpi-list']);
        $patcher = unserialize($_SESSION['maciaslPatcher']);

        $table = $acpiList->getAcpiTableObj('');

        $table->setAcpiTable($patcher->getPatchedTable()->getAcpiTable());
        $acpiList->update($acpiList->getActiveIdx(), $table);

        $_SESSION['acpi-list'] = serialize($acpiList);
        $re = $table->getAcpiTable();
    }
        break;
    default:
        break;
}

echo json_encode($re);
exit(0);

exit_error:
    echo json_encode(false);
    exit(1);