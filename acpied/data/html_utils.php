<?php
/*
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * License - http://www.gnu.org/licenses/gpl-3.0.txt
 */
declare(strict_types = 1);

require_once __DIR__.'/user_fn.php';

function drawAlert(string $type, string $msg, string $handle='', bool $status=true): string {
    $hidden = $status ? '':'d-none';

    return "<div class=\"alert alert-{$type} afixdalert alert-dismissible fade show {$handle} {$hidden}\" role=\"alert\">
                {$msg}
                <button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"alert\" aria-label=\"Close\"></button>
            </div>";
}

function drawHead(): string {
    global $text;

    $tok = genCsrfToken();
    
    return "<meta charset=\"utf-8\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
            <meta name=\"csrf-token\" content=\"{$tok}\">
            <meta name=\"description\" content=\"Web ACPI editor with MaciASL patches support\">
            <meta name=\"author\" content=\"kylon\">
            <title>{$text['title']}</title>";
}

function drawFileHandles(ACPIE\acpiContainer $acpiList, string $curIdx): string {
    $list = $acpiList->getIndexesList();
    $html = '';
    
    foreach ($list as $idx) {
        $name = substr($idx, 0, strlen($idx)-7);
        $active = $idx === $curIdx ? ' fhandle-active':'';

        $html .= "<div class=\"file-handle {$active}\" title=\"{$idx}\" data-idx=\"{$idx}\" data-click=\"switch-table\">
                    <i class=\"fa icon-file\"></i>
                    <div class=\"handle-name\">{$name}</div>
                    <i class=\"fa icon-times handle-close\" data-click=\"close-table\"></i>
                 </div>";
    }
    
    return $html;
}

function drawToolsBar(): string {
    global $text;

    // icon => [text, event class]
    $toolbar = [
        'tree'        => [$text['acpi_tree'], 'tree'],
        'folder-open' => [$text['open'], 'openf'],
        'wrench'      => [$text['patch'], 'patch'],
        'hammer'      => [$text['compile'], 'compile'],
        'download'    => [$text['download'], 'savedialog'],
        'gear'        => [$text['settings'], 'settings'],
        'info'        => [$text['info'], 'info'],
    ];
    $html = '';
    
    foreach ($toolbar as $icon => $vals) {
        $html .= "<div class=\"menu-btn\" data-type=\"{$vals[1]}\" title=\"{$vals[0]}\">
                    <i class=\"fa icon-{$icon}\"></i >
                  </div>";
    }
    
    return "<div class=\"toolbar\">
                {$html}
                <input type=\"file\" class=\"opentbin d-none\" value=\"\" accept=\"{$text['supported_ext']}\">
            </div>";
}

function drawTableTree(?\ACPIE\acpiTable $table): string {
    $tableTree = $table !== null ? $table->getHTMLTableTree() : '';

    return "<div id=\"acpitree\">
                <span class=\"tree\">{$tableTree}</span>
                <div id=\"loadingtree\" class=\"text-center align-middle d-none\">
                    <div class=\"spinner-border text-danger\" role=\"status\">
                        <span class=\"visually-hidden\">Loading...</span>
                    </div>
                </div>
            </div>";
}

function drawEditor(?\ACPIE\acpiTable $table): string {
    $acpiTable = $table !== null ? $table->getAcpiTable() : '';

    return "<textarea id=\"editor\" class=\"d-none\" autofocus>{$acpiTable}</textarea>";
}

/**
 * Generate iasl binary select input
 *
 * @param string $mode ['c' - compile, 'd' - decompile]
 *
 * @return string
 */
function drawIaslArgsSel(string $mode): string {
    global $iaslCArgsTx, $iaslDArgsTx, $text;

    $args = $mode === 'c' ? $iaslCArgsTx : $iaslDArgsTx;
    $opts = '';

    foreach ($args as $arg => $desc)
        $opts = "<option value=\"{$arg}\">{$desc}</option>";

    return "<select class=\"form-select iasl-{$mode} args\">
                <option value=\"\"></option>
                <option value=\"del\">{$text['iarg_del']}</option>
                {$opts}
            </select>";
}