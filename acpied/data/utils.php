<?php
/*
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * License - http://www.gnu.org/licenses/gpl-3.0.txt
 */
declare(strict_types = 1);

require_once __DIR__.'/langs/select.php';
require_once __DIR__.'/classes/iaslBinary.php';
require_once __DIR__.'/classes/acpiTable.php';
require_once __DIR__.'/classes/iaslContainer.php';
require_once __DIR__.'/classes/acpiContainer.php';
require_once __DIR__.'/classes/settingsContainer.php';
require_once __DIR__.'/classes/maciASLPatcher.php';
require_once __DIR__.'/classes/maciASLURLPatchesParser.php';

function initAcpie(): void {
    if (isset($_SESSION['acpi-list']))
        return;

    $iaslList = new ACPIE\iaslContainer();
    $acpiList = new \ACPIE\acpiContainer();
    $settList = new \ACPIE\settingsContainer();
    $table = new \ACPIE\acpiTable();

    getIaslNativeBinaries($iaslList);

    $acpiList->add($table, '', true);
    $settList->initSettings($acpiList->getActiveIdx());

    if ($iaslList->getSize() > 0) {
        $settList->set($acpiList->getActiveIdx(), 'iasl', 0);
        $iaslList->setActiveDisassembler(0);
    }

    $_SESSION['iasl-list'] = serialize($iaslList);
    $_SESSION['acpi-list'] = serialize($acpiList);
    $_SESSION['sett-list'] = serialize($settList);
    $_SESSION['miaslUrlParser'] = serialize(new \ACPIE\maciASLURLPatchesParser());
    $_SESSION['maciaslPatcher'] = serialize(new \ACPIE\maciASLPatcher());
    $_SESSION['iasl_dargs'] = '';
    $_SESSION['try_wasm'] = $iaslList->getSize() === 0 ? 'wasm':'';
}

/**
 * Generate a list of available native iasl binaries
 *
 * @param \ACPIE\iaslContainer $iaslList
 */
function getIaslNativeBinaries(\ACPIE\iaslContainer $iaslList): void {
    $diter = new DirectoryIterator(__DIR__.'/../iasl/native');
    $isLinuxOS = PHP_OS_FAMILY === 'Linux';
    $isDarwinOS = PHP_OS_FAMILY === 'Darwin';

    if (!$diter->valid())
        return;

    foreach ($diter as $finfo) {
        if (!$finfo->isFile() || $finfo->getFilename()[0] === '.')
            continue;

        $file = $finfo->getRealPath();
        $fd = fopen($file, 'rb');

        if ($fd === false)
            continue;

        $bytes = unpack('H*', fread($fd, 4));
        $head = array_pop($bytes);
        $isLinux = strcasecmp($head, '7f454c46') === 0 && $isLinuxOS;
        $isMacOS = strcasecmp($head, 'cffaedfe') === 0 && $isDarwinOS;

        fclose($fd);

        if ($isLinux || $isMacOS) {
            $iasl = new ACPIE\iaslBinary($file, getNativeBinaryVersion($file), true);

            $iaslList->addBinary($iasl);
        }
    }

    $iaslList->sortBinariesByFileName(true);
}

/**
 * Get iasl version of native binary
 *
 * @param string $iaslPath - iasl path
 *
 * @return string
 */
function getNativeBinaryVersion(string $iaslPath): string {
    $out = [];

    exec($iaslPath, $out);

    return extractIaslVersionStr($out[5], $out[2]);
}

/**
 * Get iasl version of wasm binary
 *
 * @param string $output - iasl output
 *
 * @return string
 */
function getWasmBinaryVersion(string $output): string {
    return extractIaslVersionStr($output, $output);
}

/**
 * Extract the version string from iasl output
 *
 * @param string $latestIaslOut - output of new iasl versions
 * @param string $legacyIaslOut - output of legacy iasl versions
 *
 * @return string
 */
function extractIaslVersionStr(string $latestIaslOut, string $legacyIaslOut): string {
    $match = [];
    $ver = '';

    if (preg_match('/revision\s([\d.]+)/i', $latestIaslOut, $match))
        $ver = "iasl {$match[1]}";

    if (preg_match('/version\s([\d\-]+)/i', $legacyIaslOut, $match))
        $ver = "iasl {$match[1]}";

    return $ver !== '' ? $ver:'iasl [Unknown]';
}

/**
 * Generate select input options for iasl native binaries
 *
 * Note: Wasm options will overwrite these options (see js)
 *
 * @param \ACPIE\iaslContainer $iaslList
 * @param int $current
 *
 * @return string
 */
function genIaslNativeBinariesOptions(\ACPIE\iaslContainer $iaslList, int $current): string {
    $binaries = $iaslList->getBinariesList();
    $ret = '';
    $i = 0;

    foreach ($binaries as $iasl) {
        $selected = $current === $i ? 'selected':'';

        $ret .= "<option value=\"{$i}\" {$selected}>{$iasl['version']}</option>";
        ++$i;
    }

    return $ret;
}

function filterSupportedIaslArgs(string $argsStr, string $mode): string {
    global $iaslDArgsTx, $iaslCArgsTx;

    $argsList = explode(' ', $argsStr);
    $suppList = $mode === 'c' ? $iaslCArgsTx:$iaslDArgsTx;
    $valid = [];

    foreach ($argsList as $arg) {
        $v = false;

        foreach ($suppList as $sarg => $desc) {
            if ($arg !== $sarg)
                continue;

            $v = true;
            break;
        }

        if ($v)
            array_push($valid, $arg);
    }

    return implode(' ', $valid);
}

function byteArrayToString(array $byteArray, bool $decode = false): string {
    if ($decode)
        $byteArray = json_decode(json_encode($byteArray), true);

    $stream = fopen('php://temp', 'wb+');

    foreach ($byteArray as $byte)
        fwrite($stream, pack('C*', $byte));

    $ret = stream_get_contents($stream, -1, 0);

    fclose($stream);

    return $ret !== false ? $ret:'';
}

function stringToByteArray(string $string): array {
    $ret = [];

    for ($i=0, $l=strlen($string); $i<$l; ++$i)
        array_push($ret, unpack('C*', $string[$i])[1]);

    return $ret;
}

function genPatchesListOptions(): string {
    $urlPatchesParser = unserialize($_SESSION['miaslUrlParser']);
    $patchesList = $urlPatchesParser->getPatchesListFromCache();
    $ret = '<option value="" selected></option>';

    foreach ($patchesList as $label) {
        $t = explode(';;', $label);

        if (!isset($t[1])) {
            $ret .= "<option value=\"x\" disabled></option><option value=\"x\" disabled>{$label}</option><option value=\"x\" disabled></option>";

            continue;
        }

        $ret .= "<option value=\"{$label}\">{$t[1]}</option>";
    }

    return $ret;
}
