<?php
/*
 * Cloud ACPI Editor
 * Copyright (C) kylon 2016-2020
 * License - http://www.gnu.org/licenses/gpl-3.0.txt
 */
header("Content-Security-Policy: default-src 'self' data: ajax.cloudflare.com;");

session_start();

require_once __DIR__.'/data/links.php';
require_once __DIR__.'/data/utils.php';
require_once __DIR__.'/data/html_utils.php';

global $text, $link;

initAcpie();

$iaslList = unserialize($_SESSION['iasl-list']);
$acpiList = unserialize($_SESSION['acpi-list']);
$settList = unserialize($_SESSION['sett-list']);
$curTable = $acpiList->getAcpiTableObj('');
$curIdx = $acpiList->getActiveIdx();
$tableSignature = $curTable !== null ? $curTable->getSignature() : 'UNK';
?>
<!DOCTYPE html>
<html lang="en" class="h-100">
    <head>
        <?php echo drawHead(); ?>

        <link type="text/css" rel="stylesheet" href="<?php echo $link['bootstrap']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['icomoon']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['psscrollbar']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['cm']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['cmsimplescrollbar']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['cmdialog']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['cmmaciasl-dark']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['acpie']; ?>">
        <link type="text/css" rel="stylesheet" href="<?php echo $link['dark']; ?>">
    </head>

    <body class="<?php echo "{$_SESSION['try_wasm']}"; ?> h-100">
        <div id="opened-list">
            <?php echo drawFileHandles($acpiList, $curIdx); ?>
            <div id="newfile" data-click="add-file">
                <i class="fa icon-plus"></i>
            </div>
        </div>

        <div id="wrapper" class="h-100">
            <?php
            echo drawToolsBar();
            echo drawTableTree($curTable);
            echo drawEditor($curTable);
            ?>

            <div id="notfs" class="toast-container position-fixed top-0 end-0 p-4 d-none">
                <div id="acpietoast" class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-bs-animation="true" data-bs-autohide="true" data-bs-delay="3000">
                    <div class="toast-header text-white">
                        <strong class="me-auto">Cloud ACPI Editor</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>
                    <div class="toast-body"></div>
                </div>
            </div>

            <footer class="footer"><?php echo $text['credit']; ?></footer>
        </div>

        <?php
        if ($iaslList->getSize() === 0)
            echo drawAlert('warning', $text['no_iasl_bins'], 'no_aml', false); ?>

        <!-- SETTINGS MODAL -->
        <div id="acpie-settings" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content acpie-modal-theme">
                    <div class="modal-header">
                        <h5 class="modal-title"><?php echo $text['settings']; ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="row mb-3">
                                    <div class="col-12 sec-title">
                                        <?php echo $text['glob_sett_t']; ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <p class="fs-bold"><?php echo $text['diasl_ver']; ?>:</p>
                                        <select class="form-select diasl-rev" form="amltableE" name="iasl_dis">
                                            <option value="x">loading..</option>
                                            <?php echo genIaslNativeBinariesOptions($iaslList, $iaslList->getActiveDisassemblerIdx()); ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-12">
                                        <p class="fs-bold"><?php echo $text['iasl_dargs_t']; ?>:</p>

                                        <div>
                                            <?php echo drawIaslArgsSel('d'); ?>
                                            <textarea class="form-control iasl-udargs" name="iasl_dargs"><?php echo $_SESSION['iasl_dargs']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-6 mt-3 mt-sm-0">
                                <div class="row mb-3">
                                    <div class="col-12 sec-title">
                                        <?php echo $text['curr_sett_t']; ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <p class="fs-bold"><?php echo $text['iasl_ver']; ?>:</p>

                                        <select class="form-select iasl-rev" name="iasl">
                                            <option value="x">loading..</option>
                                            <?php echo genIaslNativeBinariesOptions($iaslList, $settList->get($curIdx, 'iasl')); ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-12">
                                        <p class="fs-bold"><?php echo $text['iasl_cargs_t']; ?>:</p>

                                        <div>
                                            <?php echo drawIaslArgsSel('c'); ?>
                                            <textarea class="form-control iasl-uargs"><?php echo $settList->get($curIdx, 'iasl_cargs'); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- SAVE AS MODAL -->
        <div id="saveas" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content acpie-modal-theme">
                    <div class="modal-header">
                        <h5 class="modal-title"><?php echo $text['save_title']; ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <input type="text" class="saveas-text" form="saveas-form" name="filename" value="" placeholder="<?php echo $tableSignature; ?>">
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="mb-3">
                                    <label><?php echo $text['file_extension']; ?></label>
                                    <select name="fextension" form="saveas-form" class="form-select">
                                        <option value="dsl">.dsl (<?php echo $text['dsl_text']; ?>)</option>
                                        <option value="aml">.aml (<?php echo $text['aml_text']; ?>)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer custom-btn-modal">
                        <form id="saveas-form" method="post" action="data/xhr/save.php" class="m-0">
                            <div class="btn save-btn" data-click="save-btn">
                                <?php echo $text['save']; ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- iASL OUTPUT MODAL -->
        <div id="acpie-iasl-out" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content acpie-modal-theme">
                    <div class="modal-header">
                        <h5 class="modal-title"><?php echo $text['out_title']; ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="acpi-iasl-output"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- MaciASL patches MODAL -->
        <div id="maciasl-patch" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content acpie-modal-theme">
                    <div class="modal-header">
                        <h5 class="modal-title"><?php echo $text['maciasl_patch_t']; ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <select id="patch-sel" class="form-select">
                                    <option value="x" class="d-none"><?php echo $text['fetching_patches']; ?></option>
                                    <?php echo genPatchesListOptions(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-12">
                                <span id="pcnt">0</span> <?php echo $text['patches']; ?>,
                                <span id="pchgs">0</span> <?php echo $text['changes']; ?>
                            </div>
                        </div>

                        <div class="row mt-2 mb-1 iaslrejcheck d-none">
                            <div class="col-12">
                                <div class="alert alert-danger" role="alert">
                                    <?php echo $text['rejected_iasl']; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-1">
                            <div id="patchesEd" class="col-12">
                                <textarea id="mpatch" class="d-none" autofocus></textarea>
                                <div id="loading-peditor" class="text-center d-none">
                                    <div class="spinner-border text-primary" role="status">
                                        <span class="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 text-end">
                                <div class="btn-group" role="group" aria-label="Apply and cancel buttons">
                                    <button type="button" class="btn btn-secondary patcher-open-btn" data-click="openupatch"><?php echo $text['open']; ?></button>
                                    <button type="button" class="btn btn-secondary patcher-build-btn" data-click="buildupatch"><?php echo $text['build']; ?></button>
                                    <button type="button" class="btn btn-secondary patcher-apply-btn" data-click="applyupatch" disabled><?php echo $text['apply']; ?></button>
                                    <input id="opatchi" class="d-none" type="file" value="" accept=".txt">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-6 text-center">
                                <div><?php echo $text['before']; ?></div>
                            </div>
                            <div class="col-6 text-center">
                                <div><?php echo $text['after']; ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer p-0 border-top-0">
                        <div class="patches-loadscr text-center w-100 mb-4 d-none">
                            <p class="buildingp d-none"><?php echo $text['buildingp']; ?></p>
                            <p class="applyingp d-none"><?php echo $text['applyingp']; ?></p>
                            <img src="../style/img/ball-triangle.svg" alt="loading">
                        </div>

                        <div class="pdelta-tb table-responsive m-0 w-100 d-none">
                            <table class="table table-hover table-light mb-0">
                                <tbody id="pdelta-cont">
                                    <tr class="d-none">
                                        <td class="w-50"><p class="w-100 bef"></p></td>
                                        <td class="w-50"><p class="bw-100 aft"></p></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- INFO MODAL -->
        <div id="acpie-info" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content acpie-modal-theme">
                    <div class="modal-header">
                        <h5 class="modal-title"><?php echo $text['title']; ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12 col-md-6 text-center">
                            	<img src="../style/img/cloudACPIE_gray.png" class="img-fluid acpie-logo rotate-vert-center-lin" alt="cloud acpi editor logo">
                            </div>
							<div class="col-12 col-md-6 text-center">
								<div class="row">
                                    <div class="col-12 info-right-title margin-top-20-mob">
                                        <?php echo $text['info_useful_links']; ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 margin-top-20">
                                        <ul class="text-start">
                                            <li>
                                                <a href="https://bitbucket.org/kylon/cloud-acpi-editor" target="_blank" rel="noopener">Cloud ACPI Editor Sources</a> <br>
                                            </li>
                                            <li>
                                                <a href="https://github.com/acpica/acpica" target="_blank" rel="noopener">ACPICA Sources</a>
                                            </li>
                                            <li>
                                                <a href="http://cloudclovereditor.altervista.org/cce/index.php" target="_blank" rel="noopener">Cloud Clover Editor</a> <br>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>

        <!-- LOADING SCREEN -->
        <div id="loadingscr" class="text-center align-middle d-none">
            <div class="spinner-border text-danger" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>
        </div>

        <script src="<?php echo $link['jquery']; ?>"></script>
        <script src="<?php echo $link['bootstrapjs']; ?>"></script>
        <script src="<?php echo $link['psscrollbarjs']; ?>"></script>
        <script src="<?php echo $link['cmjs']; ?>"></script>
        <script src="<?php echo $link['cmclosebrackets']; ?>"></script>
        <script src="<?php echo $link['cmmatchbrackets']; ?>"></script>
        <script src="<?php echo $link['cmtrailspacejs']; ?>"></script>
        <script src="<?php echo $link['cmdialogjs']; ?>"></script>
        <script src="<?php echo $link['cmsearchcursorjs']; ?>"></script>
        <script src="<?php echo $link['cmsearchjs']; ?>"></script>
        <script src="<?php echo $link['cmsimplescrollbarjs']; ?>"></script>
        <script src="<?php echo $link['cmactivelinejs']; ?>"></script>
        <script src="<?php echo $link['cmasljs']; ?>"></script>
        <script src="<?php echo $link['cmmaciaslrexjs']; ?>"></script>
        <script src="<?php echo $link['utilsjs']; ?>"></script>
        <script src="<?php echo $link['wasmiasl']; ?>"></script>
        <script src="<?php echo $link['editorjs']; ?>"></script>
    </body>
</html>
