<?php
require_once __DIR__.'/acpied/data/classes/maciASLURLPatchesParser.php';

$parser = new \ACPIE\maciASLURLPatchesParser(false);
$plistCacheF = fopen(__DIR__.'/acpied/cache/maciASLPatchesListCache.php', 'w+');
$pdataCacheF = fopen(__DIR__.'/acpied/cache/maciASLPatchesDataCache.php', 'w+');
$plistCache = "<?php\nreturn \$patchesList = [\n";
$pdataCache = "<?php\nreturn \$patchesData = [\n";

echo "ACPIE maciASLURLPatchesParser cache start...\n";

if ($plistCacheF === false || $pdataCacheF === false) {
    echo "Could not create cache files\n";
    exit(1);
}

echo "fetching patches...\n";
$parser->extractFromBuiltins();

foreach ($parser->getPatchesList() as $pl)
    $plistCache .= "'{$pl}',\n";

$plistCache .= "];";

foreach ($parser->getPatchesData() as $k => $a) {
    $pdataCache .= "'{$k}' => [\n";

    foreach ($a as $ak => $av) {
        $av = str_replace("'", "\'", $av);
        $pdataCache .= "'{$ak}' => '{$av}',\n";
    }

    $pdataCache .= "],\n";
}

$pdataCache .= "];";

echo "writing cache...\n";

$ret = fwrite($plistCacheF, $plistCache);
if ($ret === false)
    echo "fatal error: plistCache write!";
else
    fclose($plistCacheF);

$ret = fwrite($pdataCacheF, $pdataCache);
if ($ret === false)
    echo "fatal error: pdataCache write!";
else
    fclose($pdataCacheF);

echo "done\n";